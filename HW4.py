import numpy as np
from numpy import arange
import math


# def integration_uplow(a,b,n,f):
#     sums = np.empty((3))
#     #sum_up1 = 0
#     sum_l = sum_up2 =0
#     i = 0
#     if a > b:
#         a,b = b,a
#     h = (b - a)/n
#     while i*h < b-a:
#         #To calculate upper sum recursively, other than the bulk addition method used, uncomment the relevant parts
#         #x_i=a+i*h
#         x_i1 = a + (i+1)*h
#         #print(x_i)
#         #print("Xi+1")
#         #print(x_i1)
#         #print(h)
#         sum_l = sum_l + f(x_i1)*h
#         #sum_up1 = sum_up1 + f(x_i)*h
#         #print(sum_l)
#         i += 1
#     sum_up2 = sum_l + h*(f(a) - f(b))
#     sums[0] = sum_l
#     #sums[1] = sum_up1
#     sums[1] = sum_up2
#     sums[2] = (sum_l + sum_up2)/2
#     return sums

# def integration_trapezoid(a,b,n,f):
#     i = 1
#     if a > b:
#         a,b = b,a
#     h = (b - a)/n
#     sum = (f(a) + f(b))/2
#     while i*h < b-a:
#         x = a + i*h
#         sum = sum + f(x)
#         i += 1
#     sum = sum*h
#     return sum


# def integration_romberg(a,b,n,f):
#     i = 1
#     intg = np.zeros((n,n))
#     if a > b:
#         a,b = b,a
#     h = b-a
#     intg[0][0] = (h/2)*(f(a) + f(b))
#     while i < n:
#         h = h/2
#         sum = 0
#         j = k = 1
#         while k <= 2**i -1:
#             sum = sum + f(a+k*h)
#             k+=2
#         intg[i][0] = 1/2*intg[i-1][0]+sum*h
#         while j <= i:
#             intg[i][j] = intg[i][j-1]+(intg[i][j-1] - intg[i-1][j-1])/(4**j-1)
#             j +=1
#         i+=1
#     return intg



def integration_adaptivesimpson(a,b,f,error,level_max,level = 0):
    if a > b:
       a,b = b,a
    level = level + 1 
    #print(b)
    h = b - a
    c = (a + b)/2
    d = (a + c)/2
    e = (c + b)/2
    one_simpson = h*(f(a) + 4*f(c) + f(b))/6
    two_simpson = h*(f(a) + 4*f(d) + 2*f(c) + 4*f(e) + f(b))/12
    if level >= level_max:
        #print("0")
        simpson_result = two_simpson

        print("Maximum Level Reached")
    elif abs(two_simpson - one_simpson) < 15*error:
        #print("1")
        simpson_result = two_simpson + (two_simpson - one_simpson)/15

    else:
        #print("2")
        left_simpson = integration_adaptivesimpson(a,c,f,error/2,level_max,level)
        right_simpson = integration_adaptivesimpson(c,b,f,error/2,level_max,level)
        simpson_result = left_simpson + right_simpson
        #print(simpson_result)

    return simpson_result        

def function_to_integrate(x):
    #return math.exp(-1*x**2)
    #return 4/(1+x**2)
    #return abs(x)
    return (math.cos(2*x))/math.exp(x)
#print(function_to_integrate(1))

# sums = integration_uplow(-1,1,8,function_to_integrate)
# print("Sum Low, Sum Up, Sum Avg")
# print(sums)

# sum = integration_trapezoid(0,1,60,function_to_integrate)
# print(sum)

#sums = integration_romberg(0,1,5,function_to_integrate)
#print(sums)

sum = integration_adaptivesimpson(0,(5*math.pi/4),function_to_integrate,(10**-3/2),50)
print(sum)