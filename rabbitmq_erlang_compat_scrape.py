from typing import OrderedDict
import requests
from bs4 import BeautifulSoup
import json

def scrape_rabbitmq_versions():
    # Fetch the webpage
    url = "https://www.rabbitmq.com/docs/which-erlang"
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36'}
    response = requests.get(url, headers=headers)
    if response.status_code != 200:
        raise Exception(f"Failed to fetch page: {response.status_code}")
        
    soup = BeautifulSoup(response.text, 'html.parser')
    
    # Find the table in the article content
    article = soup.find('article')
    if not article:
        raise Exception("Article content not found")
        
    tables = article.find_all('table')
    if tables is None:
        raise Exception("Version compatibility tables not found")

    supported_rabbit_table = tables[0]
    unsupported_rabbit_table = tables[1]
    supported_rabbit_version_data =process_rabbit_table(supported_rabbit_table)
    unsupported_rabbit_version_data = process_rabbit_table(unsupported_rabbit_table,supported=False)
    
    # Save to JSON file
    output_file = "rabbitmq_erlang_unsupported_versions.json"
    with open(output_file, "w") as f:
        json.dump(unsupported_rabbit_version_data, f, indent=4)

    
    output_file = "rabbitmq_erlang_supported_versions.json"
    with open(output_file, "w") as f:
        json.dump(supported_rabbit_version_data, f, indent=4)
    return supported_rabbit_version_data
    
def process_rabbit_table(rabbit_table, supported=True):
    # support_data =  []
    support_data = OrderedDict()
     # Process each row in the table
    rows = rabbit_table.find_all('tr')
    print(f"Found {len(rows)} rows")  # Debug print
    
    for row in rows:  # Skip header row
        cols = row.find_all(['td', 'th'])
        if len(cols) >= 2:
            rabbitmq_versions = cols[0]
            min_erlang_versions = cols[1].text.strip()
            max_erlang_versions = cols[2].text.strip()
            if min_erlang_versions.__contains__('x'):
                min_erlang_versions = min_erlang_versions.replace('x','*')
            if max_erlang_versions.__contains__('x'):
                max_erlang_versions = max_erlang_versions.replace('x','*')
            for rabbitmq_version in rabbitmq_versions.find_all('li'):
                rabbitmq_version = rabbitmq_version.text.strip()
                
                print(f"Processing: {rabbitmq_version} - Min Erl version: {min_erlang_versions}, Max Erl version: {max_erlang_versions}")
                     
                support_data[rabbitmq_version] = {
                    "comparison_type": "value_range",
                    "supported": supported,
                    "min": min_erlang_versions,
                    "max": max_erlang_versions
                }
                # support_data = {
                #     "key"  : rabbitmq_version,
                #       "comparison_type": "value_range",
                #     "supported": supported,
                #     "min": min_erlang_versions,
                #     "max": max_erlang_versions
                # }
                # support_data.append(support_info)
    
    return support_data

if __name__ == "__main__":
    try:
        results = scrape_rabbitmq_versions()
        print(json.dumps(results, indent=4))
    except Exception as e:
        print(f"Error: {e}")
        # For debugging, print the HTML structure
        response = requests.get("https://www.rabbitmq.com/docs/which-erlang")
        soup = BeautifulSoup(response.text, 'html.parser')
        print("\nPage structure:")
        print(soup.find('article'))