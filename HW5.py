import numpy as np
from numpy import arange
import math


def naive_gauss(a,b):
    i = k = j = 0
    if a.shape[0] == a.shape[1]:
        n=a.shape[0]
        x=np.empty((n))
        print("Value in Naive Gauss")
        print(a)
        #print("Size: " + str(n))
        while k < n:
            i=k+1
            while i < n:
                xmult = a[i][k]/a[k][k]
                a[i][k] = xmult
                j = k+1
                while j < n:
                    a[i][j] = a[i][j] - xmult*a[k][j]
                    j +=1
                b[i] = b[i] - xmult*b[k]
                i+=1
            k+=1
        x[n-1] = b[n-1]/a[n-1][n-1]
        i = n-2
        while i >= 0:
            sum = b[i]
            j = i+1
            while j < n:
                sum = sum - a[i][j]*x[j]
                j+=1
            x[i] = sum/a[i][i]
            i -=1
        return x    



#s -> scale array
#l -> index array

def Gauss(a):
    i = k = 0
    if a.shape[0] == a.shape[1]:
        n=a.shape[0]
        l=np.zeros(n,dtype=np.int8)
        s=np.zeros(n)
        #print("Size and After A: " + str(n))
        print("Value in Gauss")
        print(a)
        while i < n:
            j = 0
            smax = 0
            l[i] = i
            while j < n:
                #print("Row index: " + str(i) , "Column index: " + str(j) + "Value: " +str(a[i][j]))
                smax = max(smax,abs(a[i][j]))
                j+=1
                #print("J:" +str(j))
            #print("i: " + str(i))
            #print("SMAX" + str(smax))
            s[i] = smax
            i+=1
        print("S vector")
        print(s)
        while k < n:
            rmax = 0
            i = k
            while i < n:               
                r = abs(a[l[i]][k]/s[l[i]])
                #print("R: "+str(r))
                if r > rmax:
                    rmax = r
                    j = i
                    #print(i)
                i+=1
            #print("J:" +str(j))
            #print("i: " + str(i))
            l[j],l[k] = l[k],l[j]
            i = k +1
            while i < n:       
                xmult = a[l[i]][k]/a[l[k]][k]
                a[l[i]][k] = xmult
                j = k+1
                while j < n:
                    a[l[i]][j] = a[l[i]][j] - xmult*a[l[k]][j]
                    j +=1
                i+=1
            k+=1
        return a,l

def Solve(a,l,b):
    i = k = j = 0
    if a.shape[0] == a.shape[1]:
        n=a.shape[0]
        x=np.empty((n))
        while k<n-1:
            i=k+1
            while i<n:
                b[l[i]] = b[l[i]] - a[l[i]][k]*b[l[k]]
                i+=1
            k+=1
        x[n-1] = b[l[n-1]]/a[l[n-1]][n-1]
        i = n-2
        while i>=0:
            sum = b[l[i]]
            j = i+1
            while j < n:
                sum = sum - a[l[i]][j]*x[j]
                j+=1
            x[i] = sum/a[l[i]][i]
            i -=1
        return x
            



for n in range(4,11):
    a = np.zeros(shape=(n,n))
    b = np.zeros(shape=(n))
    for i in range(1,n+1):
        for j in range(1,n+1):
            a[i-1][j-1] = (i+1)**(j-1)
        b[i-1] = ((i+1)**n -1)/i
    print("Coefficient Matrix")
    print(a)
    print("Result vector")
    print(b)
    print("Naive Gauss")
    x = naive_gauss(a,b)
    print(x)
    # a,l = Gauss(a)
    # print("Returned vector from scale")
    # print(a)
    # print("Returned pivot ranks")
    # print(l)
    # res = Solve(a,l,b)
    # print("Scaled Gaus")
    # print(res)
# a = np.matrix([[3, 4, 3], [1, 5,-1], [6,3,7]])
# b = np.array([10,7,15])


# N=3

# #INITIALIZATION of 7 x 2 array with deafult value as 0
# ar=[[0]*N for x in range(N)]

# #RECEIVING NEW VALUES TO THE INITIALIZED ARRAY
# for i in range(N):
#     for j in range(N):
#         ar[i][j]=int(input())
# #print(ar)
# ar = np.matrix(ar)


# res = naive_gauss(ar,b)
# print(res)
