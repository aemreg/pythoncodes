# Even though it is forbidden to use AI in the Dune universe, a Fremen Chani and Paul, attempt to do the unacceptable and learn the language of the machines: Coding.

# After they honed their coding skills a bit, they noticed that they can use this to understand different types of sandworms. Chani has identified the different sandworms in Arrakis with her computer vision model, and that this model aggregates info on each sandworm in a json file. She tells Paul that he can use this JSON file to create an detailed scrapbook.

# Paul wants his scrapbook to be created in a particular way, however he does not know how to process this JSON file to form his scrapbook properly. Can you help Paul with writing his processing code?

# Here is some information about the sandworms, and their attributes:

# There are  types of sandworms: ancient, elder, earth, air, divine, dark, electric, colossal.

# Each sandworms has an age between  and .

# Each sandworms has a unique name.

# Each sandworms has its own roar. A roar is what a sandworm says, similar to cats saying meow.

# There are  sandworms in the JSON file.  is the maximum number of words for any sandworms in the JSON file.

# Paul wants his scrapbook to be formed within particular rules:

# From each type such as ancient, elder, air etc. a maximum of  sandworms will be included in the scrapbook. ( sandworms from ancient category,  sandworms from elder category ...)

# This  sandworm free space has a priority for older sandworms. If the  sandworm space is not enough for all sandworms in a category to be included, only the oldest sandworms will make it inside the scrapbook. (Oldest  sandworms from ancient category, oldest  sandworms from elder category ...)

# There is also a space for each sandworm’s roar. If the roar is longer than  words, only the first  words of the roar will make it into the scrapbook, for each sandworm. ( words of roar for the first sandworm in the scrapbook,  words of roar for the first sandworm in the scrapbook ...)

# If the roar is shorter than  words, free space remaining for the roar will be filled with the word \, as many as the length of the free space.




#!/bin/python
import csv
import math
import os
import random
import re
import sys
import json
from sys import stdout
#from collections import Counter
if __name__ == '__main__':
    # Get input parameters T and R

    first_multiple_input = raw_input().rstrip().split()

    T = int(first_multiple_input[0])

    R = int(first_multiple_input[1])
    if(T > 0 and T <= 250 and R > 0 and R <= 1000):
    # Get the content of JSON file as string
        sandworms = raw_input()
        sandworms_dict = json.loads(sandworms)
        if( len(sandworms_dict) <= 4000 and len(sandworms_dict) > 0 ):
    # Write your code here
    #print(type(sandworms_dict))
    #print(type(sandworms_dict[0]))
            sandworms_dict_sorted = sorted(sandworms_dict, key=lambda d: (d['sandworm_type'],-d['sandworm_age']))
    #print(sandworms_dict_sorted)
    #sw_groupby_count = Counter(sw['sandworm_type'] for sw in sandworms_dict_sorted)
    #print(sw_groupby_count)
            tsv_writer = csv.writer(stdout, delimiter = '\t')
            tsv_writer.writerow(['sandworm_type', 'sandworm_age', 'what_does_the_sandworm_say', 'sandworm_name'])
            freq = {"ancient" : 0, "elder" : 0, "earth" : 0, "air" : 0, "divine" : 0, "dark" : 0, "electric" : 0, "colossal" : 0}
            for item in sandworms_dict_sorted:
                if (freq[item['sandworm_type']] < T):
                    freq[item['sandworm_type']] += 1
            #print(item['sandworm_age'])
                    say =item['what_does_the_sandworm_say'].split(' ',R)
            #print(test)
                    if(len(say) < R):
                        concat_str = ['<SILENCE>'] * (R-len(say))
                        say = say + concat_str
                        say = " ".join(say)

                    elif (len(say) >=R and len(say) < 10000 ):
                        say = say[:R]
                        say = " ".join(say)
                    tsv_writer.writerow([item['sandworm_type'].encode('utf-8'), item['sandworm_age'], say.encode('utf-8'), item['sandworm_name'].encode('utf-8')])
            
    #print(freq)
