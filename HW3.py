import numpy as np
from numpy import arange
import math


def derivative(f,x,n,h):
    d = np.zeros((n,n))
    i = 0
    while i < n:
        j = 1
        d[i][0] = (f(x+h) - f(x-h))/(2*h)
        #print("I: " + str(i) + ", Val: " + str(d[i][0]) + ", H:" + str(h))
        while j <= i:
            d[i][j] = d[i][j-1] + (d[i][j-1] - d[i-1][j-1])/(4**j -1)
            #d[i,j] = d[i,j-1]+(d[i,j-1]-d[i-1,j-1])/((4**j)-1)
            j+=1
        h = h/2
        #print(h)
        i+=1
    return d


# def func(x):
#     #print(math.sin(x))
#     return math.sin(x)

# n = 20
# h = 1
# x = 1.2309594154

# return_array = derivative(func,x,n,h)

# #print(return_array[4][0])
# #print(return_array[4][1])
# #print(math.sin(x))
# print(math.cos(x))
# for x in abs(return_array-math.cos(x)):
#     str_to_print = ""
#     for y in x:
#         if y > 0:
#             str_to_print = str_to_print + str(y) + ", "
#     print(str_to_print)



def second_derivate(f,x,n,h):
    d = np.zeros((n))
    i = 0
    while i < n:
        d[i] = (f(x+h) + f(x-h) - 2*f(x))/(h**2)
        h = h/2
        i+=1
    return d


def second_derivate_richardson(f,x,n,h):
    d = np.zeros((n,n))
    i = 0
    while i < n:
        j = 1
        d[i][0] = (f(x+h) + f(x-h) - 2*f(x))/(h**2)
        #print("I: " + str(i) + ", Val: " + str(d[i][0]) + ", H:" + str(h))
        while j <= i:
            d[i][j] = d[i][j-1] + (d[i][j-1] - d[i-1][j-1])/(4**j -1)
            j+=1
        h = h/2
        #print(h)
        i+=1
    return d

def func(x):
    #print(math.sin(x))
    # return math.sin(x)
    # return math.cos(x)
    return np.arctan(x)

n = 6
h = 1
x = 1

diff_l1 = derivative(func,x,n,h)
diff_l2 = second_derivate_richardson(func,x,n,h)
print(diff_l1)
print(diff_l2)

