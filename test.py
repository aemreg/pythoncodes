import numpy as np
from numpy import arange
import math


# def integration_uplow(a,b,n,f):
#     sums = np.empty((3))
#     #sum_up1 = 0
#     sum_l = sum_up2 =0
#     i = 0
#     if a > b:
#         a,b = b,a
#     h = (b - a)/n
#     while i*h < b-a:
#         #To calculate upper sum recursively, other than the bulk addition method used, uncomment the relevant parts
#         #x_i=a+i*h
#         x_i1 = a + (i+1)*h
#         #print(x_i)
#         #print("Xi+1")
#         #print(x_i1)
#         #print(h)
#         sum_l = sum_l + f(x_i1)*h
#         #sum_up1 = sum_up1 + f(x_i)*h
#         #print(sum_l)
#         i += 1
#     sum_up2 = sum_l + h*(f(a) - f(b))
#     sums[0] = sum_l
#     #sums[1] = sum_up1
#     sums[1] = sum_up2
#     sums[2] = (sum_l + sum_up2)/2
#     return sums

# def integration_trapezoid(a,b,n,f):
#     i = 1
#     if a > b:
#         a,b = b,a
#     h = (b - a)/n
#     sum = (f(a) + f(b))/2
#     while i*h < b-a:
#         x = a + i*h
#         sum = sum + f(x)
#         i += 1
#     sum = sum*h
#     return sum


# def integration_romberg(a,b,n,f):
#     i = 1
#     intg = np.zeros((n,n))
#     if a > b:
#         a,b = b,a
#     h = b-a
#     intg[0][0] = (h/2)*(f(a) + f(b))
#     while i < n:
#         h = h/2
#         sum = 0
#         j = k = 1
#         while k <= 2**i -1:
#             sum = sum + f(a+k*h)
#             k+=2
#         intg[i][0] = 1/2*intg[i-1][0]+sum*h
#         while j <= i:
#             intg[i][j] = intg[i][j-1]+(intg[i][j-1] - intg[i-1][j-1])/(4**j-1)
#             j +=1
#         i+=1
#     return intg



# def integration_adaptivesimpson(a,b,f,error,level_max,level = 0):
#     if a > b:
#        a,b = b,a
#     level = level + 1 
#     #print(b)
#     h = b - a
#     c = (a + b)/2
#     d = (a + c)/2
#     e = (c + b)/2
#     one_simpson = h*(f(a) + 4*f(c) + f(b))/6
#     two_simpson = h*(f(a) + 4*f(d) + 2*f(c) + 4*f(e) + f(b))/12
#     if level >= level_max:
#         #print("0")
#         simpson_result = two_simpson

#         print("Maximum Level Reached")
#     elif abs(two_simpson - one_simpson) < 15*error:
#         #print("1")
#         simpson_result = two_simpson + (two_simpson - one_simpson)/15

#     else:
#         #print("2")
#         left_simpson = integration_adaptivesimpson(a,c,f,error/2,level_max,level)
#         right_simpson = integration_adaptivesimpson(c,b,f,error/2,level_max,level)
#         simpson_result = left_simpson + right_simpson
#         #print(simpson_result)

#     return simpson_result        

# def function_to_integrate(x):
#     #return math.exp(-1*x**2)
#     #return 4/(1+x**2)
#     #return abs(x)
#     return (math.cos(2*x))/math.exp(x)
#print(function_to_integrate(1))

# sums = integration_uplow(-1,1,8,function_to_integrate)
# print("Sum Low, Sum Up, Sum Avg")
# print(sums)

# sum = integration_trapezoid(0,1,60,function_to_integrate)
# print(sum)

#sums = integration_romberg(0,1,5,function_to_integrate)
#print(sums)

# sum = integration_adaptivesimpson(0,(5*math.pi/4),function_to_integrate,(10**-3/2),50)
# print(sum)




# def testfunc(a):
#     if a.shape[0] == a.shape[1]:
#         print('YAY')
#         print(a[0][0])
#     print(a.shape)
#     print(len(a))

# a = np.matrix('1 2; 3 4 ')
# a = np.matrix('3 4 3; 1 5 -1; 6 3 7')
# a = []
# a[0] = (3,4,3)
# a[1] = (1,5,-1)
# a[0] = (6,3,7)

# N=3

# #INITIALIZATION of 7 x 2 array with deafult value as 0
# ar=[[0]*N for x in range(N)]

# #RECEIVING NEW VALUES TO THE INITIALIZED ARRAY
# for i in range(N):
#     for j in range(N):
#         ar[i][j]=int(input())
# #print(ar)
# ar = np.matrix(ar)
# print(ar)
# testfunc(ar)




# b = np.zeros(shape=(5,2))
# testfunc(b)


# l = [1,2,3,4]

# k = np.array([9,8,7,6,5,4,3,2,1,0,1,2,3,4,5,6,7,8,9])

# print(l[3])
# print(k[l[3]])


# a1 = np.array([[0.780, 0.563], [0.913,0.659]])
# b = np.array([[0.217], [0.254]])
# x_tilda = np.array([[0.999], [-1.001]])
# x_head = np.array([[0.341], [-0.087]])
# x = np.array([[1], [-1]])
# # print(a1)
# # print(b)
# print(np.matmul(a1,x))

# b_head = np.matmul(a1,x_head) 
# b_tilda = np.matmul(a1,x_tilda)
# print(b_head)
# print(b_tilda)

# r_head = b - np.matmul(a1,x_head) 
# r_tilda = b - np.matmul(a1,x_tilda)
# print(r_head)
# print(r_tilda)


# vector1 = [0.780, 0.563]
# vector2 = [0.913,0.659]

# unit_vector1 = vector1 / np.linalg.norm(vector1)
# unit_vector2 = vector2 / np.linalg.norm(vector2)

# dot_product = np.dot(unit_vector1, unit_vector2)

# angle = np.arccos(dot_product)/np.pi*180 #angle in radian

# print(angle)


def Gauss(a):
  
    if a.shape[0] == a.shape[1]:
        i = k = 0
        n=a.shape[0]
        l=np.zeros(n,dtype=np.int8)
        s=np.zeros(n)
        #print("Size and After A: " + str(n))
        #print("Value in Gauss")
        #print(a)
        while i < n:
            j = 0
            smax = 0
            l[i] = i
            while j < n:
                #print("Row index: " + str(i) , "Column index: " + str(j) + "Value: " +str(a[i][j]))
                smax = max(smax,abs(a[i][j]))
                j+=1
                #print("J:" +str(j))
            #print("i: " + str(i))
            #print("SMAX" + str(smax))
            s[i] = smax
            i+=1
        print("S vector")
        print(s)
        while k < n:
            print(l)
            rmax = 0
            i = k
            while i < n:               
                r = abs(a[l[i]][k]/s[l[i]])
                #print("R: "+str(r))
                if r > rmax:
                    rmax = r
                    temp = i
                    #print(i)
                i+=1
            #print("J:" +str(j))
            #print("i: " + str(i))
            l[temp],l[k] = l[k],l[temp]
            i = k +1
            while i < n:       
                xmult = a[l[i]][k]/a[l[k]][k]
                #print("index i: " + str(i) + ", index j: "+ str(j)+ ", index k: "+ str(k) +", xmult: " + str(xmult))
                a[l[i]][k] = xmult
                #print(a)
                j = k+1
                while j < n:
                    a[l[i]][j] = a[l[i]][j] - xmult*a[l[k]][j]
                    #print("index i: " + str(i) + ", index j: "+ str(j)+ ", index k: "+ str(k) +", xmult: " + str(xmult))
                    #print(a)
                    j +=1
                i+=1
            k+=1
        return a,l

def Solve(a,l,b):
    i = k = j = 0
    if a.shape[0] == a.shape[1]:
        n=a.shape[0]
        x=np.empty((n),dtype=np.complex128)
        while k<n-1:
            i=k+1
            while i<n:
                b[l[i]] = b[l[i]] - a[l[i]][k]*b[l[k]]
                i+=1
            k+=1
        x[n-1] = b[l[n-1]]/a[l[n-1]][n-1]
        i = n-2
        while i>=0:
            sum = b[l[i]]
            j = i+1
            while j < n:
                sum = sum - a[l[i]][j]*x[j]
                j+=1
            x[i] = sum/a[l[i]][i]
            i -=1
        return x

def naive_gauss(a,b):
    i = k = j = 0
    if a.shape[0] == a.shape[1]:
        n=a.shape[0]
        x=np.empty((n),dtype=np.complex128)
        print("Value in Naive Gauss")
        print(a)
        #print("Size: " + str(n))
        while k < n:
            i=k+1
            while i < n:
                xmult = a[i][k]/a[k][k]
                a[i][k] = xmult
                j = k+1
                while j < n:
                    a[i][j] = a[i][j] - xmult*a[k][j]
                    j +=1
                b[i] = b[i] - xmult*b[k]
                i+=1
            k+=1
        x[n-1] = b[n-1]/a[n-1][n-1]
        i = n-2
        while i >= 0:
            sum = b[i]
            j = i+1
            while j < n:
                sum = sum - a[i][j]*x[j]
                j+=1
            x[i] = sum/a[i][i]
            i -=1
        return x     

#a1 = np.array([[0.780, 0.563], [0.913,0.659]])
#a1 = np.array([[0.913,0.659], [0.780, 0.563]],dtype=np.float64)
a1 = np.array([[3,-13,9,3], [-6, 4, 1,-18],[6,-2,2,4],[12,-8,6,10]],dtype=np.complex128)
new_a, l = Gauss(a1)
#b = np.array([[0.217], [0.254]])
#b = np.array([[0.254],[0.217]])
b = np.array([[-19],[-34],[16],[26]],dtype=np.complex128)
x = Solve(new_a,l,b)
#print(a1)
print(l)
print(new_a)
print(x)

# a1 = np.array([[0.913,0.659], [0.780, 0.563]])
# #b = np.array([[0.217], [0.254]])
# b = np.array([[0.254],[0.217]])
# x2 = naive_gauss(a1,b)

# print(x2)