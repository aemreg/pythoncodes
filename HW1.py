import math

# i = h = 1
# n= imax = 30
# error = y = x = 0.5
# emax = 0
# while i < n:
#     h = h*0.25
#     y = (math.sin(x+h) - math.sin(x))/h
#     error = abs(math.cos(x) -y)
#     print("Error at {} step is {} , last increment is on {}, max error: {}".format(i,error,imax,emax))
#     i += 1
#     if error > emax:
#         emax = error
#         imax = i


# while i < n:
#     h = h*0.25
#     y = ((x+h)**3 - x**3)/h
#     error = abs(3*x**2 -y)
#     print("Error at {} step is {} , last increment is on {}, max error: {}".format(i,error,imax,emax))
#     i += 1
#     if error > emax:
#         emax = error
#         imax = i



def func(x):
    return math.exp(x)-3*x*x

def secant(a,b,f,nmax,emax):
    n = 2
    f_a = func(a)
    f_b = func(b)
    #check values
    while(n<nmax):
        d = (b - a)/(f_b - f_a)
        b = a
        f_b = f_a 
        d = d*f_a
        if abs(d) < emax:
            print("Convergence")
        a = a - d
        f_a=f(a)
        print("Value of n: {}",format(n))
        print("Value of a: {}",format(a))
        print("Value of f_a: {}",format(f_a))
        n += 1
secant(-0.5,1.0,func,15,0.000000005)




# def func(x):
#     return math.exp(x)-3*x*x

# def secant(f,a,b,epsi,delta,nmax):
#     n = 2
#     f_a = func(a)
#     f_b = func(b)
#     #compare values  
#     if abs(f_a) > abs(f_b):
#         a, b = b,a
#         f_a,f_b = f_b,f_a
#     while(n<nmax):
#         cur_eps = 0
#         d = (b - a)/(f_b - f_a)
#         b = a
#         f_b = f_a 
#         d = d*f_a
#         ###Check if epsilon convergence(changes between x values, numericaly unstable if too small) is reached, if so break loop
#         if abs(d) < epsi:
#             print("Epsilon Convergence")
#             break
#         a = a - d
#         f_a=f(a)
#         ###Check if delta convergence(closeness to y = 0) is reached, if so break loop
#         if abs(f_a) < delta:
#             print("Delta Convergence")
#             break
#         print("Value of n: {}",format(n))
#         print("Value of a: {}",format(a))
#         print("Value of f_a: {}",format(f_a))
#         n += 1


# secant(f = func,a =-0.5,b = 1,epsi = 1e-12,delta = 1e-6,nmax = 30)