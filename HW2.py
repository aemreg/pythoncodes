#Test_Chebishev
import numpy
from numpy import arange
import math

### initialize de points and the size of the intervals
def lin_array(start,end , size):
    start , end , size = start,end,size
    step = abs(end-start)/(size-1)
    #print(step)
    x =arange(start,end+step,step)
    return(x)
#print(len(x))
def func(x):
    #function we are trying to estimate
    #y = []
    y = (x**2 + 1)**(-1)
    # for i in x:
    #     temp = math.sin(i)
    #     y.append(temp)
    return y


def Coef(n,x,y):
    a = y
    j = 1
    while j<=n:
        i = n
        #print("J:" + str(j))
        #print("J's i:" + str(i))
        while i>=j:
            #print("İ:"+str(i))
            #print("Y valus:"+str(a[i]) +", " + str(a[i-1]) + "X values:" + str(x[i]) +", " +str(x[i-j]))
            a[i]=(a[i] - a[i-1])/(x[i] - x[i-j])
            #print(a[i])
            i -= 1
        j += 1
    return a

def Eval(n,x,a,t):
    temp = a[n]
    i = n-1
    while i>=0:
        temp = temp*(t-x[i]) + a[i]
        i -= 1
    return temp

def chebyshevpoint(x):
    y = []
    for i in x:
        temp = 5*math.cos(i*math.pi/20)
        y.append(temp)
    return y

def chebyshevpoint2(x):
    y = []
    for i in x:
        temp = 5*math.cos((2*i+1)*math.pi/42)
        y.append(temp)
    return y



# start,end,size = -5,5,21
# n = size -1
# x = lin_array(start,end,size)
# y = func(x)
# print(x)
# print(y)
# #x = [1,-4,0]
# #y = [3,13,-23]
# a = Coef(n,x,y)
# print(a)
# #test = Eval(n,x,a,x[5])
# #print(test)
# #print(y[5])
# #print(a)
# j = emax = jmax = pmax = tmax = 0
# print ("J ,T,Val,P,E")
# while j < 41:
#     step = abs(end-start)/(41-1)
#     t = start + j*step
#     p = Eval(n,x,a,t)
#     e = abs(((t**2 + 1)**(-1))-p)
#     print(j,t,((t**2 + 1)**(-1)),p,e)
#     # print("J: "+str(j))
#     # print("T: "+str(t))
#     # print("P: "+str(p))
#     # print("E: "+str(e))
#     if e > emax:
#         emax = e
#         jmax =j
#         tmax = t
#         pmax = p
#     j+=1
# print("Jmax:" + str(jmax) + ", Emax:" +str(emax) + ", Pmax:" + str(pmax) + ", Tmax:" + str(tmax))



####Chebyshew nodes
cheb_x = list(range(21))
#print(cheb_x)
#cheb_points = chebyshevpoint(cheb_x)
cheb_points = chebyshevpoint2(cheb_x)
print(cheb_points)

start,end,size = -5,5,21
n = size -1
x = lin_array(start,end,size)
y = func(x)
#print(x)
#print(y)

a = Coef(n,x,y)
j = emax = jmax = pmax = tmax = 0
print ("J ,T,Val,P,E")
for j in cheb_x:
    t = cheb_points[j]
    p = Eval(n,x,a,t)
    e = abs(((t**2 + 1)**(-1))-p)
    print(j,t,((t**2 + 1)**(-1)),p,e)
    # print("J: "+str(j))
    # print("T: "+str(t))
    # print("P: "+str(p))
    # print("E: "+str(e))
    if e > emax:
        emax = e
        jmax =j
        tmax = t
        pmax = p
print("Jmax:" + str(jmax) + ", Emax:" +str(emax) + ", Pmax:" + str(pmax) + ", Tmax:" + str(tmax))