{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from time import time\n",
    "import matplotlib\n",
    "import sys\n",
    "# the following will prevent the figure from popping up\n",
    "matplotlib.use('Agg')\n",
    "from matplotlib import pyplot as plt\n",
    "def simple_mandelbrot(width, height, real_low, real_high, imag_low, imag_high, max_iters, upper_bound):\n",
    "    \n",
    "     real_vals = np.linspace(real_low, real_high, width)\n",
    "     imag_vals = np.linspace(imag_low, imag_high, height)\n",
    "        \n",
    "     # we will represent members as 1, non-members as 0.\n",
    "    \n",
    "     mandelbrot_graph = np.ones((height,width), dtype=np.float32)\n",
    "    \n",
    "     for x in range(width):\n",
    "        \n",
    "         for y in range(height):\n",
    "            \n",
    "             c = np.complex64( real_vals[x] + imag_vals[y] * 1j  )           \n",
    "             z = np.complex64(0)\n",
    "            \n",
    "             for i in range(max_iters):\n",
    "                \n",
    "                 z = z**2 + c\n",
    "                \n",
    "                 if(np.abs(z) > upper_bound):\n",
    "                     mandelbrot_graph[y,x] = 0\n",
    "                     break\n",
    "                \n",
    "     return mandelbrot_graph"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[ 0.  0.  0.  0.  0.]\n",
      " [ 0.  0.  1.  0.  0.]\n",
      " [ 1.  1.  1.  0.  0.]\n",
      " [ 0.  0.  1.  0.  0.]\n",
      " [ 0.  0.  0.  0.  0.]]\n",
      "It took 0.00317907333374 seconds to calculate the Mandelbrot graph.\n",
      "It took 0.0409219264984 seconds to dump the image.\n"
     ]
    }
   ],
   "source": [
    "t1 = time()\n",
    "#mandel = simple_mandelbrot(512,512,-2,2,-2,2,256)\n",
    "gpu_mandel = simple_mandelbrot(5,5,-2,2,-2,2,10,16)\n",
    "t2 = time()\n",
    "mandel_time = t2 - t1\n",
    "print(gpu_mandel)\n",
    "t1 = time()\n",
    "fig = plt.figure(1)\n",
    "plt.imshow(gpu_mandel, extent=(-2, 2, -2, 2))\n",
    "#plt.savefig('mandelbrot.png', dpi=fig.dpi)\n",
    "t2 = time()\n",
    "    \n",
    "dump_time = t2 - t1\n",
    "    \n",
    "print 'It took {} seconds to calculate the Mandelbrot graph.'.format(mandel_time)\n",
    "print 'It took {} seconds to dump the image.'.format(dump_time)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "It took 0.00321412086487 seconds to calculate the Mandelbrot graph.\n"
     ]
    }
   ],
   "source": [
    "print('It took {} seconds to calculate the Mandelbrot graph.'.format(mandel_time))\n",
    "#print 'It took {} seconds to dump the image.'.format(dump_time)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "test\n"
     ]
    }
   ],
   "source": [
    "print('test')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "CUDA device query (PyCUDA version) \n",
      "\n",
      "Detected 1 CUDA Capable device(s) \n",
      "\n",
      "Device 0: NVIDIA Tegra X1\n",
      "\t Compute Capability: 5.3\n",
      "\t Total Memory: 3964 megabytes\n",
      "\t (1) Multiprocessors, (128) CUDA Cores / Multiprocessor: 128 CUDA Cores\n",
      "\t MAXIMUM_TEXTURE2D_LINEAR_PITCH: 1048544\n",
      "\t MAXIMUM_TEXTURE2D_GATHER_WIDTH: 16384\n",
      "\t MAXIMUM_TEXTURE2D_GATHER_HEIGHT: 16384\n",
      "\t PCI_DEVICE_ID: 0\n",
      "\t MAXIMUM_TEXTURE3D_WIDTH: 4096\n",
      "\t MAXIMUM_SURFACE2D_WIDTH: 65536\n",
      "\t MAXIMUM_TEXTURE1D_MIPMAPPED_WIDTH: 16384\n",
      "\t GLOBAL_MEMORY_BUS_WIDTH: 64\n",
      "\t LOCAL_L1_CACHE_SUPPORTED: 1\n",
      "\t MAXIMUM_SURFACE3D_DEPTH: 4096\n",
      "\t MAXIMUM_TEXTURE3D_HEIGHT: 4096\n",
      "\t PCI_DOMAIN_ID: 0\n",
      "\t COMPUTE_CAPABILITY_MINOR: 3\n",
      "\t MULTI_GPU_BOARD_GROUP_ID: 0\n",
      "\t MAX_REGISTERS_PER_BLOCK: 32768\n",
      "\t MAXIMUM_TEXTURE2D_ARRAY_WIDTH: 16384\n",
      "\t COMPUTE_CAPABILITY_MAJOR: 5\n",
      "\t MAXIMUM_SURFACE2D_LAYERED_HEIGHT: 16384\n",
      "\t MAXIMUM_TEXTURE1D_LAYERED_LAYERS: 2048\n",
      "\t UNIFIED_ADDRESSING: 1\n",
      "\t MAXIMUM_TEXTURE1D_LINEAR_WIDTH: 134217728\n",
      "\t MAXIMUM_SURFACE3D_WIDTH: 4096\n",
      "\t MAXIMUM_SURFACE2D_HEIGHT: 65536\n",
      "\t ECC_ENABLED: 0\n",
      "\t MAXIMUM_TEXTURE2D_ARRAY_NUMSLICES: 2048\n",
      "\t MAX_GRID_DIM_Y: 65535\n",
      "\t MAX_GRID_DIM_X: 2147483647\n",
      "\t MAX_GRID_DIM_Z: 65535\n",
      "\t SURFACE_ALIGNMENT: 512\n",
      "\t MAXIMUM_TEXTURE3D_DEPTH_ALTERNATE: 16384\n",
      "\t MAXIMUM_SURFACE1D_LAYERED_LAYERS: 2048\n",
      "\t TOTAL_CONSTANT_MEMORY: 65536\n",
      "\t MAXIMUM_SURFACE1D_LAYERED_WIDTH: 16384\n",
      "\t MAXIMUM_SURFACECUBEMAP_LAYERED_LAYERS: 2046\n",
      "\t MAXIMUM_TEXTURE3D_HEIGHT_ALTERNATE: 2048\n",
      "\t MAXIMUM_SURFACE2D_LAYERED_LAYERS: 2048\n",
      "\t MAXIMUM_TEXTURECUBEMAP_WIDTH: 16384\n",
      "\t GPU_OVERLAP: 1\n",
      "\t MAXIMUM_TEXTURE3D_DEPTH: 4096\n",
      "\t CAN_MAP_HOST_MEMORY: 1\n",
      "\t INTEGRATED: 1\n",
      "\t MAXIMUM_SURFACE1D_WIDTH: 16384\n",
      "\t MAX_THREADS_PER_MULTIPROCESSOR: 2048\n",
      "\t MAXIMUM_TEXTURE2D_MIPMAPPED_HEIGHT: 16384\n",
      "\t STREAM_PRIORITIES_SUPPORTED: 1\n",
      "\t MAXIMUM_SURFACECUBEMAP_WIDTH: 16384\n",
      "\t MAXIMUM_TEXTURE1D_LAYERED_WIDTH: 16384\n",
      "\t PCI_BUS_ID: 0\n",
      "\t GLOBAL_L1_CACHE_SUPPORTED: 1\n",
      "\t TEXTURE_ALIGNMENT: 512\n",
      "\t ASYNC_ENGINE_COUNT: 1\n",
      "\t MAX_PITCH: 2147483647\n",
      "\t MAXIMUM_SURFACE2D_LAYERED_WIDTH: 16384\n",
      "\t CLOCK_RATE: 921600\n",
      "\t MAXIMUM_TEXTURE2D_HEIGHT: 65536\n",
      "\t MAXIMUM_SURFACE3D_HEIGHT: 4096\n",
      "\t MAXIMUM_TEXTURECUBEMAP_LAYERED_WIDTH: 16384\n",
      "\t TEXTURE_PITCH_ALIGNMENT: 32\n",
      "\t MAXIMUM_TEXTURE3D_WIDTH_ALTERNATE: 2048\n",
      "\t MAXIMUM_SURFACECUBEMAP_LAYERED_WIDTH: 16384\n",
      "\t MAXIMUM_TEXTURE2D_MIPMAPPED_WIDTH: 16384\n",
      "\t MAXIMUM_TEXTURE2D_LINEAR_WIDTH: 65536\n",
      "\t TCC_DRIVER: 0\n",
      "\t MAXIMUM_TEXTURECUBEMAP_LAYERED_LAYERS: 2046\n",
      "\t WARP_SIZE: 32\n",
      "\t CONCURRENT_KERNELS: 1\n",
      "\t MAX_BLOCK_DIM_Z: 64\n",
      "\t MAXIMUM_TEXTURE2D_ARRAY_HEIGHT: 16384\n",
      "\t MAX_THREADS_PER_BLOCK: 1024\n",
      "\t L2_CACHE_SIZE: 262144\n",
      "\t MAXIMUM_TEXTURE2D_LINEAR_HEIGHT: 65536\n",
      "\t KERNEL_EXEC_TIMEOUT: 1\n",
      "\t COMPUTE_MODE: DEFAULT\n",
      "\t MANAGED_MEMORY: 1\n",
      "\t MULTI_GPU_BOARD: 0\n",
      "\t MAXIMUM_TEXTURE2D_WIDTH: 65536\n",
      "\t MAX_SHARED_MEMORY_PER_BLOCK: 49152\n",
      "\t MAX_BLOCK_DIM_Y: 1024\n",
      "\t MAX_BLOCK_DIM_X: 1024\n",
      "\t MAX_SHARED_MEMORY_PER_MULTIPROCESSOR: 65536\n",
      "\t MAXIMUM_TEXTURE1D_WIDTH: 65536\n",
      "\t MEMORY_CLOCK_RATE: 12750\n",
      "\t MAX_REGISTERS_PER_MULTIPROCESSOR: 65536\n"
     ]
    }
   ],
   "source": [
    "import pycuda\n",
    "import pycuda.driver as drv\n",
    "drv.init()\n",
    "\n",
    "print 'CUDA device query (PyCUDA version) \\n'\n",
    "\n",
    "print 'Detected {} CUDA Capable device(s) \\n'.format(drv.Device.count())\n",
    "\n",
    "for i in range(drv.Device.count()):\n",
    "    \n",
    "    gpu_device = drv.Device(i)\n",
    "    print 'Device {}: {}'.format( i, gpu_device.name() ) \n",
    "    compute_capability = float( '%d.%d' % gpu_device.compute_capability() )\n",
    "    print '\\t Compute Capability: {}'.format(compute_capability)\n",
    "    print '\\t Total Memory: {} megabytes'.format(gpu_device.total_memory()//(1024**2))\n",
    "    \n",
    "    # The following will give us all remaining device attributes as seen \n",
    "    # in the original deviceQuery.\n",
    "    # We set up a dictionary as such so that we can easily index\n",
    "    # the values using a string descriptor.\n",
    "    \n",
    "    device_attributes_tuples = gpu_device.get_attributes().iteritems() \n",
    "    device_attributes = {}\n",
    "    \n",
    "    for k, v in device_attributes_tuples:\n",
    "        device_attributes[str(k)] = v\n",
    "    \n",
    "    num_mp = device_attributes['MULTIPROCESSOR_COUNT']\n",
    "    \n",
    "    # Cores per multiprocessor is not reported by the GPU!  \n",
    "    # We must use a lookup table based on compute capability.\n",
    "    # See the following:\n",
    "    # http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#compute-capabilities\n",
    "    \n",
    "    cuda_cores_per_mp = { 5.0 : 128, 5.1 : 128, 5.2 : 128, 5.3 : 128,6.0 : 64, 6.1 : 128, 6.2 : 128}[compute_capability]\n",
    "    \n",
    "    print '\\t ({}) Multiprocessors, ({}) CUDA Cores / Multiprocessor: {} CUDA Cores'.format(num_mp, cuda_cores_per_mp, num_mp*cuda_cores_per_mp)\n",
    "    \n",
    "    device_attributes.pop('MULTIPROCESSOR_COUNT')\n",
    "    \n",
    "    for k in device_attributes.keys():\n",
    "        print '\\t {}: {}'.format(k, device_attributes[k])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "env: PATH=/usr/local/cuda/bin:/usr/local/cuda/bin:/home/aeg/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin\n",
      "env: LD_LIBRARY_PATH=/usr/local/cuda/lib64\n"
     ]
    }
   ],
   "source": [
    "%env PATH=/usr/local/cuda/bin:/usr/local/cuda/bin:/home/aeg/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin\n",
    "%env LD_LIBRARY_PATH=/usr/local/cuda/lib64"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[  3.   9.  15.  21.  27.  33.]\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "import pycuda.autoinit\n",
    "from pycuda import gpuarray\n",
    "host_data = np.array([1,3,5,7,9,11],dtype=np.float32)\n",
    "device_data = gpuarray.to_gpu(host_data)\n",
    "device_data_x2 = 3*device_data\n",
    "host_data_x2 = device_data_x2.get()\n",
    "print(host_data_x2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'CLICOLOR': '1',\n",
       " 'DBUS_SESSION_BUS_ADDRESS': 'unix:path=/run/user/1000/bus',\n",
       " 'GIT_PAGER': 'cat',\n",
       " 'HOME': '/home/aeg',\n",
       " 'JETSON_BOARD': 'P3449-0000',\n",
       " 'JETSON_BOARDIDS': '3448',\n",
       " 'JETSON_CHIP_ID': '33',\n",
       " 'JETSON_CODENAME': 'porg',\n",
       " 'JETSON_CUDA': '10.2.89',\n",
       " 'JETSON_CUDA_ARCH_BIN': '5.3',\n",
       " 'JETSON_CUDNN': '8.0.0.180',\n",
       " 'JETSON_JETPACK': '4.4.1',\n",
       " 'JETSON_L4T': '32.4.4',\n",
       " 'JETSON_L4T_RELEASE': '32',\n",
       " 'JETSON_L4T_REVISION': '4.4',\n",
       " 'JETSON_MACHINE': 'NVIDIA Jetson Nano (Developer Kit Version)',\n",
       " 'JETSON_MODULE': 'P3448-0000',\n",
       " 'JETSON_OPENCV': '4.1.1',\n",
       " 'JETSON_OPENCV_CUDA': 'NO',\n",
       " 'JETSON_SERIAL_NUMBER': '1422819075558',\n",
       " 'JETSON_SOC': 'tegra210',\n",
       " 'JETSON_TENSORRT': '7.1.3.0',\n",
       " 'JETSON_TYPE': 'Nano (Developer Kit Version)',\n",
       " 'JETSON_VISIONWORKS': '1.6.0.501',\n",
       " 'JETSON_VPI': '0.4.4',\n",
       " 'JETSON_VULKAN_INFO': '1.2.70',\n",
       " 'JPY_PARENT_PID': '10872',\n",
       " 'LANG': 'en_US.UTF-8',\n",
       " 'LC_ADDRESS': 'tr_TR.UTF-8',\n",
       " 'LC_IDENTIFICATION': 'tr_TR.UTF-8',\n",
       " 'LC_MEASUREMENT': 'tr_TR.UTF-8',\n",
       " 'LC_MONETARY': 'tr_TR.UTF-8',\n",
       " 'LC_NAME': 'tr_TR.UTF-8',\n",
       " 'LC_NUMERIC': 'tr_TR.UTF-8',\n",
       " 'LC_PAPER': 'tr_TR.UTF-8',\n",
       " 'LC_TELEPHONE': 'tr_TR.UTF-8',\n",
       " 'LC_TIME': 'tr_TR.UTF-8',\n",
       " 'LD_LIBRARY_PATH': '/usr/local/cuda/lib64',\n",
       " 'LESSCLOSE': '/usr/bin/lesspipe %s %s',\n",
       " 'LESSOPEN': '| /usr/bin/lesspipe %s',\n",
       " 'LOGNAME': 'aeg',\n",
       " 'LS_COLORS': 'rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.zst=01;31:*.tzst=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.wim=01;31:*.swm=01;31:*.dwm=01;31:*.esd=01;31:*.jpg=01;35:*.jpeg=01;35:*.mjpg=01;35:*.mjpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:',\n",
       " 'MAIL': '/var/mail/aeg',\n",
       " 'MPLBACKEND': 'module://ipykernel.pylab.backend_inline',\n",
       " 'OLDPWD': '/home/aeg',\n",
       " 'PAGER': 'cat',\n",
       " 'PATH': '/usr/local/cuda/bin:/usr/local/cuda/bin:/home/aeg/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin',\n",
       " 'PWD': '/home/aeg/cuda',\n",
       " 'SHELL': '/bin/bash',\n",
       " 'SHLVL': '1',\n",
       " 'SSH_CLIENT': '192.168.31.125 61610 22',\n",
       " 'SSH_CONNECTION': '192.168.31.125 61610 192.168.31.126 22',\n",
       " 'SSH_TTY': '/dev/pts/0',\n",
       " 'TERM': 'xterm-color',\n",
       " 'USER': 'aeg',\n",
       " 'XDG_DATA_DIRS': '/usr/local/share:/usr/share:/var/lib/snapd/desktop',\n",
       " 'XDG_RUNTIME_DIR': '/run/user/1000',\n",
       " 'XDG_SESSION_ID': '3',\n",
       " '_': '/home/aeg/.local/bin/jupyter-lab'}"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%env"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "total time to compute on CPU: 0.159297\n",
      "total time to compute on GPU: 0.119669\n",
      "Is the host computation the same as the GPU computation? : True\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "import pycuda.autoinit\n",
    "from pycuda import gpuarray\n",
    "from time import time\n",
    "host_data = np.float32( np.random.random(50000000) )\n",
    "\n",
    "t1 = time()\n",
    "host_data_2x =  host_data * np.float32(2)\n",
    "t2 = time()\n",
    "\n",
    "print 'total time to compute on CPU: %f' % (t2 - t1)\n",
    "device_data = gpuarray.to_gpu(host_data)\n",
    "\n",
    "t1 = time()\n",
    "device_data_2x =  device_data * np.float32( 2 )\n",
    "t2 = time()\n",
    "\n",
    "from_device = device_data_2x.get()\n",
    "print 'total time to compute on GPU: %f' % (t2 - t1)\n",
    "\n",
    "print 'Is the host computation the same as the GPU computation? : {}'.format(np.allclose(from_device, host_data_2x) )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('time_calc0.py','r') as f:\n",
    "     time_calc_code = f.read()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "total time to compute on CPU: 0.191219\n",
      "total time to compute on GPU: 0.289821\n",
      "Is the host computation the same as the GPU computation? : True\n",
      " "
     ]
    },
    {
     "data": {
      "text/plain": [
       "         272 function calls in 4.270 seconds\n",
       "\n",
       "   Ordered by: cumulative time\n",
       "\n",
       "   ncalls  tottime  percall  cumtime  percall filename:lineno(function)\n",
       "        1    0.000    0.000    4.892    4.892 <string>:1(<module>)\n",
       "        1    0.010    0.010    1.833    1.833 numeric.py:2397(allclose)\n",
       "        1    0.258    0.258    1.808    1.808 numeric.py:2463(isclose)\n",
       "        1    1.642    1.642    1.642    1.642 {method 'random_sample' of 'mtrand.RandomState' objects}\n",
       "        1    1.009    1.009    1.523    1.523 numeric.py:2522(within_tol)\n",
       "        2    0.514    0.257    0.514    0.257 {abs}\n",
       "        2    0.456    0.228    0.456    0.228 gpuarray.py:1231(_memcpy_discontig)\n",
       "        1    0.000    0.000    0.381    0.381 gpuarray.py:1047(to_gpu)\n",
       "        2    0.337    0.168    0.337    0.168 gpuarray.py:162(__init__)\n",
       "        1    0.000    0.000    0.231    0.231 gpuarray.py:265(get)\n",
       "        1    0.000    0.000    0.226    0.226 gpuarray.py:230(set)\n",
       "        1    0.000    0.000    0.181    0.181 gpuarray.py:485(__mul__)\n",
       "        1    0.000    0.000    0.181    0.181 gpuarray.py:412(_new_like_me)\n",
       "        3    0.000    0.000    0.041    0.014 fromnumeric.py:1973(all)\n",
       "        3    0.000    0.000    0.041    0.014 {method 'all' of 'numpy.ndarray' objects}\n",
       "        3    0.000    0.000    0.041    0.014 _methods.py:40(_all)\n",
       "        3    0.041    0.014    0.041    0.014 {method 'reduce' of 'numpy.ufunc' objects}\n",
       "        6    0.000    0.000    0.001    0.000 iostream.py:382(write)\n",
       "        9    0.000    0.000    0.001    0.000 iostream.py:195(schedule)\n",
       "        3    0.000    0.000    0.000    0.000 stride_tricks.py:38(as_strided)\n",
       "        9    0.000    0.000    0.000    0.000 socket.py:357(send)\n",
       "        3    0.000    0.000    0.000    0.000 __init__.py:576(wrapper)\n",
       "        1    0.000    0.000    0.000    0.000 gpuarray.py:351(_axpbz)\n",
       "        1    0.000    0.000    0.000    0.000 driver.py:547(function_prepared_async_call)\n",
       "        2    0.000    0.000    0.000    0.000 gpuarray.py:225(flags)\n",
       "        1    0.000    0.000    0.000    0.000 gpuarray.py:19(_get_common_dtype)\n",
       "        2    0.000    0.000    0.000    0.000 gpuarray.py:1217(_compact_strides)\n",
       "        2    0.000    0.000    0.000    0.000 array.py:68(__init__)\n",
       "        6    0.000    0.000    0.000    0.000 {method 'decode' of 'str' objects}\n",
       "        3    0.000    0.000    0.000    0.000 numeric.py:463(asarray)\n",
       "        2    0.000    0.000    0.000    0.000 gpuarray.py:109(splay)\n",
       "       12    0.000    0.000    0.000    0.000 {numpy.core.multiarray.array}\n",
       "        1    0.000    0.000    0.000    0.000 {numpy.core.multiarray.empty}\n",
       "        6    0.000    0.000    0.000    0.000 iostream.py:320(_schedule_flush)\n",
       "        1    0.000    0.000    0.000    0.000 characterize.py:21(has_double_support)\n",
       "        2    0.000    0.000    0.000    0.000 array.py:59(is_f_contiguous_strides)\n",
       "        2    0.000    0.000    0.000    0.000 numeric.py:2667(seterr)\n",
       "        6    0.000    0.000    0.000    0.000 utf_8.py:15(decode)\n",
       "        2    0.000    0.000    0.000    0.000 {sorted}\n",
       "        1    0.000    0.000    0.000    0.000 array.py:76(get_common_dtype)\n",
       "       21    0.000    0.000    0.000    0.000 {isinstance}\n",
       "        2    0.000    0.000    0.000    0.000 <decorator-gen-152>:1(_splay_backend)\n",
       "        1    0.000    0.000    0.000    0.000 numeric.py:3069(__exit__)\n",
       "        4    0.000    0.000    0.000    0.000 array.py:48(equal_strides)\n",
       "        6    0.000    0.000    0.000    0.000 {_codecs.utf_8_decode}\n",
       "        1    0.000    0.000    0.000    0.000 numeric.py:3064(__enter__)\n",
       "        2    0.000    0.000    0.000    0.000 __init__.py:532(_deco)\n",
       "        2    0.000    0.000    0.000    0.000 array.py:63(is_c_contiguous_strides)\n",
       "        6    0.000    0.000    0.000    0.000 iostream.py:307(_is_master_process)\n",
       "        1    0.000    0.000    0.000    0.000 <decorator-gen-130>:1(get_axpbz_kernel)\n",
       "        9    0.000    0.000    0.000    0.000 threading.py:986(isAlive)\n",
       "        1    0.000    0.000    0.000    0.000 tools.py:416(context_dependent_memoize)\n",
       "        3    0.000    0.000    0.000    0.000 numeric.py:534(asanyarray)\n",
       "        1    0.000    0.000    0.000    0.000 {pycuda._pvt_struct.pack}\n",
       "        2    0.000    0.000    0.000    0.000 numeric.py:2767(geterr)\n",
       "        2    0.000    0.000    0.000    0.000 {numpy.core.multiarray.zeros}\n",
       "        1    0.000    0.000    0.000    0.000 numeric.py:2135(isscalar)\n",
       "        9    0.000    0.000    0.000    0.000 iostream.py:93(_event_pipe)\n",
       "        2    0.000    0.000    0.000    0.000 array.py:28(f_contiguous_strides)\n",
       "        2    0.000    0.000    0.000    0.000 array.py:38(c_contiguous_strides)\n",
       "        4    0.000    0.000    0.000    0.000 gpuarray.py:1220(<genexpr>)\n",
       "        2    0.000    0.000    0.000    0.000 {numpy.core.umath.seterrobj}\n",
       "        6    0.000    0.000    0.000    0.000 {posix.getpid}\n",
       "        1    0.000    0.000    0.000    0.000 {numpy.core.multiarray.result_type}\n",
       "        3    0.000    0.000    0.000    0.000 {getattr}\n",
       "        1    0.000    0.000    0.000    0.000 numeric.py:3060(__init__)\n",
       "        3    0.000    0.000    0.000    0.000 stride_tricks.py:20(__init__)\n",
       "        4    0.000    0.000    0.000    0.000 {zip}\n",
       "        3    0.000    0.000    0.000    0.000 stride_tricks.py:25(_maybe_view_as_subclass)\n",
       "        4    0.000    0.000    0.000    0.000 {numpy.core.umath.geterrobj}\n",
       "        1    0.000    0.000    0.000    0.000 {method 'format' of 'str' objects}\n",
       "       22    0.000    0.000    0.000    0.000 {len}\n",
       "        9    0.000    0.000    0.000    0.000 threading.py:570(isSet)\n",
       "        4    0.000    0.000    0.000    0.000 {time.time}\n",
       "        9    0.000    0.000    0.000    0.000 {method 'append' of 'collections.deque' objects}\n",
       "        4    0.000    0.000    0.000    0.000 {method 'pop' of 'dict' objects}\n",
       "        2    0.000    0.000    0.000    0.000 {setattr}\n",
       "        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "%prun -s cumulative exec(time_calc_code)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pycuda.autoinit\n",
    "from pycuda import gpuarray\n",
    "from time import time\n",
    "from pycuda.elementwise import ElementwiseKernel\n",
    "host_data = np.float32( np.random.random(50000000) )\n",
    "gpu_2x_ker = ElementwiseKernel(\n",
    "\"float *in, float *out\",\n",
    "\"out[i] = 2*in[i];\",\n",
    "\"gpu_2x_ker\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def speedcomparison():\n",
    "    t1 = time()\n",
    "    host_data_2x =  host_data * np.float32(2)\n",
    "    t2 = time()\n",
    "    print 'total time to compute on CPU: %f' % (t2 - t1)\n",
    "    device_data = gpuarray.to_gpu(host_data)\n",
    "    # allocate memory for output\n",
    "    device_data_2x = gpuarray.empty_like(device_data)\n",
    "    t1 = time()\n",
    "    gpu_2x_ker(device_data, device_data_2x)\n",
    "    t2 = time()\n",
    "    from_device = device_data_2x.get()\n",
    "    print 'total time to compute on GPU: %f' % (t2 - t1)\n",
    "    print 'Is the host computation the same as the GPU computation? : {}'.format(np.allclose(from_device, host_data_2x) )\n",
    "\n",
    "if __name__ == '__main__':\n",
    "    speedcomparison()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mandel_ker = ElementwiseKernel(\n",
    "\"pycuda::complex<float> *lattice, float *mandelbrot_graph, int max_iters, float upper_bound\",\n",
    "\"\"\"\n",
    "mandelbrot_graph[i] = 1;\n",
    "pycuda::complex<float> c = lattice[i]; \n",
    "pycuda::complex<float> z(0,0);\n",
    "for (int j = 0; j < max_iters; j++)\n",
    "    {  \n",
    "     z = z*z + c;\n",
    "     if(abs(z) > upper_bound)\n",
    "         {\n",
    "          mandelbrot_graph[i] = 0;\n",
    "          break;\n",
    "         }\n",
    "    }         \n",
    "\"\"\",\n",
    "\"mandel_ker\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def gpu_mandelbrot(width, height, real_low, real_high, imag_low, imag_high, max_iters, upper_bound):\n",
    "    real_vals = np.matrix(np.linspace(real_low, real_high, width), dtype=np.complex64)\n",
    "    imag_vals = np.matrix(np.linspace( imag_high, imag_low, height), dtype=np.complex64) * 1j\n",
    "    mandelbrot_lattice = np.array(real_vals + imag_vals.transpose(), dtype=np.complex64)\n",
    "    print(mandelbrot_lattice)\n",
    "    print 'Array shape:{}.'.format(mandelbrot_lattice.shape)\n",
    "    # copy complex lattice to the GPU\n",
    "    mandelbrot_lattice_gpu = gpuarray.to_gpu(mandelbrot_lattice)    \n",
    "    # allocate an empty array on the GPU\n",
    "    mandelbrot_graph_gpu = gpuarray.empty(shape=mandelbrot_lattice.shape, dtype=np.float32)\n",
    "    #mandelbrot_graph_host =np.ones_like(mandelbrot_lattice,dtype=np.float32)\n",
    "    #mandelbrot_graph_gpu = gpuarray.to_gpu(mandelbrot_graph_host)\n",
    "    #print(mandelbrot_graph_host)\n",
    "    mandel_ker( mandelbrot_lattice_gpu, mandelbrot_graph_gpu, np.int32(max_iters), np.float32(upper_bound))          \n",
    "    mandelbrot_graph = mandelbrot_graph_gpu.get()\n",
    "    return mandelbrot_graph"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#import numpy as np\n",
    "#from time import time\n",
    "#import matplotlib\n",
    "#import sys\n",
    "#from matplotlib import pyplot as plt\n",
    "# the following will prevent the figure from popping up\n",
    "matplotlib.use('Agg')\n",
    "t1 = time()\n",
    "#gpu_mandel = gpu_mandelbrot(5,5,-2,2,-2,2,10,16)\n",
    "gpu_mandel = gpu_mandelbrot(1024,1024,-2,2,-2,2,256, 2)\n",
    "t2 = time()\n",
    "mandel_time = t2 - t1\n",
    "t1 = time()\n",
    "\n",
    "fig = plt.figure(1)\n",
    "plt.imshow(gpu_mandel, extent=(-2, 2, -2, 2))\n",
    "print(gpu_mandel)\n",
    "plt.savefig('gpu_mandelbrot.png', dpi=fig.dpi)\n",
    "t2 = time()\n",
    "dump_time = t2 - t1\n",
    "\n",
    "print 'It took {} seconds to calculate the Mandelbrot graph.'.format(mandel_time)\n",
    "print 'It took {} seconds to dump the image.'.format(dump_time)    \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pycuda.autoinit\n",
    "from pycuda import gpuarray\n",
    "from pycuda.scan import InclusiveScanKernel\n",
    "seq = np.array([1,2,3,4],dtype=np.int32)\n",
    "seq_gpu = gpuarray.to_gpu(seq)\n",
    "sum_gpu = InclusiveScanKernel(np.int32, \"a+b\")\n",
    "print sum_gpu(seq_gpu).get()\n",
    "print np.cumsum(seq)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pycuda.autoinit\n",
    "from pycuda import gpuarray\n",
    "from pycuda.scan import InclusiveScanKernel\n",
    "seq = np.array([1,100,-3,-10000, 4, 10000, 66, 14, 21],dtype=np.int32)\n",
    "seq_gpu = gpuarray.to_gpu(seq)\n",
    "max_gpu = InclusiveScanKernel(np.int32, \"a > b ? a : b\")\n",
    "#print max_gpu(seq_gpu).get()[-1]\n",
    "print max_gpu(seq_gpu).get()\n",
    "print np.max(seq)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pycuda.reduction import ReductionKernel\n",
    "dot_prod = ReductionKernel(np.float32, neutral=\"0\", reduce_expr=\"a+b\", map_expr=\"vec1[i]*vec2[i]\", arguments=\"float *vec1, float *vec2\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#complex_absolute_compare = ReductionKernel(np.complex64, neutral=\"0\", reduce_expr=\"abs(a) > abs(b) ? a : b\", \n",
    "#map_expr=\"\"\"\n",
    "#pycuda::complex<float> first = vec3[i]; \n",
    "#pycuda::complex<float> second = vec4[i];\n",
    "#return_vec[i] = first;\n",
    "#if(abs(first) < abs(second))\n",
    "#         {\n",
    "#          return_vec[i] = second;\n",
    "#         }\n",
    "#    } \n",
    "#\n",
    "#\"\"\",arguments=\"pycuda::complex<float> *vec3, pycuda::complex<float> *vec4,pycuda::complex<float> *return_vec\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "complex_absolute_compare = ReductionKernel(np.float32, neutral=\"0\", reduce_expr=\"a*a > b*b ? a : b\", map_expr=\"vec1[i]*vec1[i] > vec2[i]*vec2[i] ? vec1[i] : vec2[i]\", arguments=\"float *vec1, float *vec2\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def gpu_absolute_compare(width, real_low, real_high, imag_low, imag_high):\n",
    "    real_vals = np.matrix(np.linspace(real_low, real_high, width), dtype=np.float32)\n",
    "    imag_vals = np.matrix(np.linspace( imag_high, imag_low, width), dtype=np.float32)\n",
    "    real_vals_array_gpu = gpuarray.to_gpu(real_vals)\n",
    "    imag_vals_array_gpu = gpuarray.to_gpu(imag_vals)\n",
    "    res = complex_absolute_compare( real_vals_array_gpu, imag_vals_array_gpu).get()  \n",
    "    print(res)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gpu_absolute_compare(2048,-2,2,0,5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pycuda.autoinit\n",
    "import pycuda.driver as drv\n",
    "import numpy as np\n",
    "from pycuda import gpuarray\n",
    "from pycuda.compiler import SourceModule\n",
    "ker = SourceModule(\"\"\"\n",
    "__global__ void scalar_multiply_kernel(float *outvec, float scalar, float *vec)\n",
    "{\n",
    " int i = threadIdx.x;\n",
    " outvec[i] = scalar*vec[i];\n",
    "}\n",
    "\"\"\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scalar_multiply_gpu = ker.get_function(\"scalar_multiply_kernel\")\n",
    "testvec = np.random.randn(512).astype(np.float32)\n",
    "testvec_gpu = gpuarray.to_gpu(testvec)\n",
    "outvec_gpu = gpuarray.empty_like(testvec_gpu)\n",
    "scalar_multiply_gpu( outvec_gpu, np.float32(2), testvec_gpu, block=(512,1,1), grid=(1,1,1))\n",
    "print \"Does our kernel work correctly? : {}\".format(np.allclose(outvec_gpu.get() , 2*testvec) )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "#%matplotlib widget\n",
    "import matplotlib\n",
    "import pycuda.autoinit\n",
    "import pycuda.driver as drv\n",
    "from pycuda import gpuarray\n",
    "from pycuda.compiler import SourceModule\n",
    "import numpy as np\n",
    "#import matplotlib.backends.backend_tkagg\n",
    "import matplotlib.pyplot as plt \n",
    "import matplotlib.animation as animation\n",
    "from matplotlib.animation import FuncAnimation, PillowWriter\n",
    "matplotlib.use(\"Agg\")\n",
    "ker = SourceModule(\"\"\"\n",
    "#define _X  ( threadIdx.x + blockIdx.x * blockDim.x )\n",
    "#define _Y  ( threadIdx.y + blockIdx.y * blockDim.y )\n",
    "#define _WIDTH  ( blockDim.x * gridDim.x )\n",
    "#define _HEIGHT ( blockDim.y * gridDim.y  )\n",
    "#define _XM(x)  ( (x + _WIDTH) % _WIDTH )\n",
    "#define _YM(y)  ( (y + _HEIGHT) % _HEIGHT )\n",
    "#define _INDEX(x,y)  ( _XM(x)  + _YM(y) * _WIDTH )\n",
    "// return the number of living neighbors for a given cell                \n",
    "__device__ int nbrs(int x, int y, int * in)\n",
    "{\n",
    "     return ( in[ _INDEX(x -1, y+1) ] + in[ _INDEX(x-1, y) ] + in[ _INDEX(x-1, y-1) ] \\\n",
    "                   + in[ _INDEX(x, y+1)] + in[_INDEX(x, y - 1)] \\\n",
    "                   + in[ _INDEX(x+1, y+1) ] + in[ _INDEX(x+1, y) ] + in[ _INDEX(x+1, y-1) ] );\n",
    "}\n",
    "__global__ void conway_ker(int * lattice_out, int * lattice  )\n",
    "{\n",
    "   // x, y are the appropriate values for the cell covered by this thread\n",
    "   int x = _X, y = _Y;\n",
    "   \n",
    "   // count the number of neighbors around the current cell\n",
    "   int n = nbrs(x, y, lattice);\n",
    "                   \n",
    "    \n",
    "    // if the current cell is alive, then determine if it lives or dies for the next generation.\n",
    "    if ( lattice[_INDEX(x,y)] == 1)\n",
    "       switch(n)\n",
    "       {\n",
    "          // if the cell is alive: it remains alive only if it has 2 or 3 neighbors.\n",
    "          case 2:\n",
    "          case 3: lattice_out[_INDEX(x,y)] = 1;\n",
    "                  break;\n",
    "          default: lattice_out[_INDEX(x,y)] = 0;                   \n",
    "       }\n",
    "    else if( lattice[_INDEX(x,y)] == 0 )\n",
    "         switch(n)\n",
    "         {\n",
    "            // a dead cell comes to life only if it has 3 neighbors that are alive.\n",
    "            case 3: lattice_out[_INDEX(x,y)] = 1;\n",
    "                    break;\n",
    "            default: lattice_out[_INDEX(x,y)] = 0;         \n",
    "         }\n",
    "         \n",
    "}\n",
    "\"\"\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "conway_ker = ker.get_function(\"conway_ker\")    \n",
    "def update_gpu(frameNum, img, newLattice_gpu, lattice_gpu, N):\n",
    "    conway_ker(  newLattice_gpu, lattice_gpu, grid=(N/32,N/32,1), block=(32,32,1)   ) \n",
    "    img.set_data(newLattice_gpu.get() )\n",
    "    lattice_gpu[:] = newLattice_gpu[:] \n",
    "    return img"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 46,
   "metadata": {},
   "outputs": [],
   "source": [
    "N = 128\n",
    "lattice = np.int32( np.random.choice([1,0], N*N, p=[0.25, 0.75]).reshape(N, N) )\n",
    "lattice_gpu = gpuarray.to_gpu(lattice)\n",
    "newLattice_gpu = gpuarray.empty_like(lattice_gpu)        \n",
    "fig, ax = plt.subplots()\n",
    "img = ax.imshow(lattice_gpu.get(), interpolation='nearest')\n",
    "#ani = FuncAnimation(fig, update_gpu, fargs=(img, newLattice_gpu, lattice_gpu, N, ) , interval=0, frames=1000, 0)    \n",
    "#ani = FuncAnimation(fig=fig, func =update_gpu, fargs=(img, newLattice_gpu, lattice_gpu, N, ), interval=40, frames=np.arange(0, 1000))\n",
    "ani = animation.FuncAnimation(fig, update_gpu, fargs=(img, newLattice_gpu, lattice_gpu, N, ) , interval=40, frames=1000, save_count=1000)   \n",
    "#plt.show()\n",
    "#plt.savefig('conway.png', dpi=fig.dpi)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 47,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0"
      ]
     },
     "execution_count": 47,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "np.arange(0, 1000)[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 48,
   "metadata": {},
   "outputs": [],
   "source": [
    "#matplotlib.rcParams['animation.ffmpeg_path'] = '/usr/bin/ffmpeg'\n",
    "#matplotlib.rcParams['animation.convert_path'] = '/usr/bin/convert'\n",
    "#ani.save(\"conway.gif\", writer=PillowWriter())\n",
    "Writer = animation.writers['ffmpeg']\n",
    "writer = Writer(fps = 10, bitrate = -1)\n",
    "ani.save('ani_test.mp4', writer = writer)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pycuda.autoinit\n",
    "import pycuda.driver as drv\n",
    "from pycuda import gpuarray\n",
    "from pycuda.compiler import SourceModule\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Let's first look at x and y, \n",
    "#two integers that hold the Cartesian coordinates of a particular thread's cell. \n",
    "#Remember that we are setting their values with the _X and _Y macros. \n",
    "#(Compiler optimizations notwithstanding, we want to store these values in variables to \n",
    "# reduce computation because directly using _X and _Y will recompute the x and y values every \n",
    "# time these macros are referenced in our code):\n",
    "\n",
    "#int x = _X, y = _Y; "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "ker = SourceModule(\"\"\"\n",
    "#define _X  ( threadIdx.x + blockIdx.x * blockDim.x )\n",
    "#define _Y  ( threadIdx.y + blockIdx.y * blockDim.y )\n",
    "#define _WIDTH  ( blockDim.x * gridDim.x )\n",
    "#define _HEIGHT ( blockDim.y * gridDim.y  )\n",
    "#define _XM(x)  ( (x + _WIDTH) % _WIDTH )\n",
    "#define _YM(y)  ( (y + _HEIGHT) % _HEIGHT )\n",
    "#define _INDEX(x,y)  ( _XM(x)  + _YM(y) * _WIDTH )\n",
    "// return the number of living neighbors for a given cell                \n",
    "__device__ int nbrs(int x, int y, int * in)\n",
    "{\n",
    "     return ( in[ _INDEX(x -1, y+1) ] + in[ _INDEX(x-1, y) ] + in[ _INDEX(x-1, y-1) ] \\\n",
    "                   + in[ _INDEX(x, y+1)] + in[_INDEX(x, y - 1)] \\\n",
    "                   + in[ _INDEX(x+1, y+1) ] + in[ _INDEX(x+1, y) ] + in[ _INDEX(x+1, y-1) ] );\n",
    "}\n",
    "__global__ void conway_ker(int * lattice, int iters)\n",
    "{\n",
    " int x = _X, y = _Y; \n",
    " for (int i = 0; i < iters; i++)\n",
    " {\n",
    "     int n = nbrs(x, y, lattice); \n",
    "     int cell_value;\n",
    "      if ( lattice[_INDEX(x,y)] == 1)\n",
    " switch(n)\n",
    " {\n",
    " // if the cell is alive: it remains alive only if it has 2 or 3 neighbors.\n",
    " case 2:\n",
    " case 3: cell_value = 1;\n",
    " break;\n",
    " default: cell_value = 0; \n",
    " }\n",
    " else if( lattice[_INDEX(x,y)] == 0 )\n",
    " switch(n)\n",
    " {\n",
    " // a dead cell comes to life only if it has 3 neighbors that are alive.\n",
    " case 3: cell_value = 1;\n",
    " break;\n",
    " default: cell_value = 0; \n",
    " } \n",
    " __syncthreads();\n",
    " lattice[_INDEX(x,y)] = cell_value; \n",
    " __syncthreads();\n",
    " } \n",
    "}\n",
    "\"\"\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAP8AAAD8CAYAAAC4nHJkAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADl0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uIDIuMi41LCBodHRwOi8vbWF0cGxvdGxpYi5vcmcvSM8oowAADNxJREFUeJzt3X+o3fV9x/Hna9k1zirU1B9NY5hW7KgMjXIJgqN07WydDFTYhv4h/iFNGRUmdH+Ig9XB/rBjKv7liDM0HU7rqqIMWSqhRQojenUxxqZVK27NEhI7W3Qri7/e++N8A9f0/ji555zvyfXzfMDlfM/3fL/3884353W/Pz7f8zmpKiS15zemXYCk6TD8UqMMv9Qowy81yvBLjTL8UqMMv9Qowy81yvBLjfrNUVZOciVwD7AG+IequmOp5c9Yt6bO3Thz3O28vOeUlRUo9egzF/1q0df6eg//H//LO3Ukwyybld7em2QN8DJwBbAfeBa4vqp+tNg6sxefXM/s2HjcbX35U5tWVKPUpx0Hdi/6Wl/v4V21k7fqzaHCP8ph/2bg1ap6rareAR4Crh7h90nq0Sjh3wD8bN7z/d08SavAKOFf6NDi184hkmxJMpdk7o3/fn+E5iSN0yjh3w/MP4E/Bzhw7EJVtbWqZqtq9sxPrBmhOUnjNEr4nwUuSHJekpOA64AnxlOWpElbcVdfVb2X5GZgB4Ouvm1V9dLYKpNWmdXWKzVSP39VPQk8OaZaJPXIO/ykRhl+qVGGX2qU4ZcaZfilRo10tf94vbznlFXXHSJ9VLnnlxpl+KVGGX6pUYZfapThlxpl+KVGGX6pUYZfapThlxpl+KVGGX6pUYZfapThlxpl+KVGGX6pUYZfapThlxpl+KVGGX6pUSON4ZfkdeBt4H3gvaqaHUdRkiZvHAN4/n5V/XwMv0dSjzzslxo1avgL+F6S55JsGUdBkvox6mH/5VV1IMlZwFNJflxVT89foPujsAXgZE4ZsTlJ4zLSnr+qDnSPh4HHgM0LLLO1qmaranaGtaM0J2mMVhz+JB9LctrRaeBLwN5xFSZpskY57D8beCzJ0d/zT1X1r2OpSkvacWD3ca/j16TpWCsOf1W9Blw8xlok9ciuPqlRhl9qlOGXGmX4pUYZfqlR4/hgjyZgqe68lXTbjfv3afVzzy81yvBLjTL8UqMMv9Qowy81yqv9J6ilrsD7wR6Ng3t+qVGGX2qU4ZcaZfilRhl+qVGGX2qUXX2rkN12Ggf3/FKjDL/UKMMvNcrwS40y/FKjDL/UqGXDn2RbksNJ9s6bty7JU0le6R5Pn2yZksZtmD3/t4Arj5l3K7Czqi4AdnbPJa0iy4a/qp4G3jxm9tXA9m56O3DNmOuSNGErPec/u6oOAnSPZ42vJEl9mPjtvUm2AFsATuaUSTcnaUgr3fMfSrIeoHs8vNiCVbW1qmaranaGtStsTtK4rTT8TwA3dtM3Ao+PpxxJfRmmq+9B4N+A30myP8lNwB3AFUleAa7onktaRZY956+q6xd56YtjrkVSj7zDT2qU4ZcaZfilRhl+qVGGX2qU4ZcaZfilRhl+qVGGX2qU4ZcaZfilRhl+qVGGX2qU4ZcaZfilRhl+qVGGX2qU4ZcaNfGhu9WvHQd2Lzj/y5/a1HMl7Vls2y9nWv837vmlRhl+qVGGX2qU4ZcaZfilRhl+qVHLdvUl2Qb8EXC4qn63m3c78BXgjW6x26rqyUkVOW4r7ZJZjN1oWo2G2fN/C7hygfl3V9Wm7mfVBF/SwLLhr6qngTd7qEVSj0Y55785yZ4k25KcPraKJPVipeG/Fzgf2AQcBO5cbMEkW5LMJZl7lyMrbE7SuK0o/FV1qKrer6oPgPuAzUssu7WqZqtqdoa1K61T0pitKPxJ1s97ei2wdzzlSOrLMF19DwKfB85Ish/4BvD5JJuAAl4HvjrBGsfOrjlNwmp7Xy0b/qq6foHZ90+gFkk98g4/qVGGX2qU4ZcaZfilRhl+qVGregDPpT6dd6J0u/Rd44ny79aJzz2/1CjDLzXK8EuNMvxSowy/1CjDLzXK8EuNMvxSowy/1CjDLzXK8EuNMvxSowy/1CjDLzXK8EuNMvxSowy/1CjDLzXK8EuNGubrujYC3wY+CXwAbK2qe5KsA74DnMvgK7v+tKp+MblSf91qGK9uNdSoNg2z538P+HpVfRa4DPhakguBW4GdVXUBsLN7LmmVWDb8VXWwqp7vpt8G9gEbgKuB7d1i24FrJlWkpPE7rnP+JOcClwC7gLOr6iAM/kAAZ427OEmTM3T4k5wKPALcUlVvHcd6W5LMJZl7lyMrqVHSBAwV/iQzDIL/QFU92s0+lGR99/p64PBC61bV1qqararZGdaOo2ZJY7Bs+JMEuB/YV1V3zXvpCeDGbvpG4PHxlydpUob5uq7LgRuAF5Mc/e6p24A7gIeT3AT8J/AnkylR0iQsG/6q+iGQRV7+4njLkdQX7/CTGmX4pUYZfqlRhl9qlOGXGjVMV9/YfOaiX7Fjx+7lFzyGn4yTxs89v9Qowy81yvBLjTL8UqMMv9Qowy81yvBLjTL8UqMMv9Qowy81yvBLjTL8UqN6/WDPy3tO8UM60gnCPb/UKMMvNcrwS40y/FKjDL/UKMMvNWrZrr4kG4FvA58EPgC2VtU9SW4HvgK80S16W1U9OalCdeLZccDxGFezYfr53wO+XlXPJzkNeC7JU91rd1fV302uPEmTMsx39R0EDnbTbyfZB2yYdGGSJuu4zvmTnAtcAuzqZt2cZE+SbUlOH3NtkiZo6PAnORV4BLilqt4C7gXOBzYxODK4c5H1tiSZSzL3LkfGULKkcRgq/ElmGAT/gap6FKCqDlXV+1X1AXAfsHmhdatqa1XNVtXsDGvHVbekES0b/iQB7gf2VdVd8+avn7fYtcDe8ZcnaVKGudp/OXAD8GKSo307twHXJ9kEFPA68NWJVNiolXSjgV1pGt4wV/t/CGSBl+zTl1Yx7/CTGmX4pUYZfqlRhl9qlOGXGtXrAJ76aLFbcXVzzy81yvBLjTL8UqMMv9Qowy81yvBLjbKr7wRlN5omzT2/1CjDLzXK8EuNMvxSowy/1CjDLzXK8EuNMvxSowy/1CjDLzXK8EuNMvxSo5b9YE+Sk4GngbXd8t+tqm8kOQ94CFgHPA/cUFXvTLJYado+Sl+jNsye/wjwhaq6mMHXcV+Z5DLgm8DdVXUB8AvgpsmVKWnclg1/DfxP93Sm+yngC8B3u/nbgWsmUqGkiRjqnD/Jmu4beg8DTwE/BX5ZVe91i+wHNkymREmTMFT4q+r9qtoEnANsBj670GILrZtkS5K5JHPvcmTllUoaq+O62l9VvwR+AFwGfDzJ0QuG5wAHFllna1XNVtXsDGtHqVXSGC0b/iRnJvl4N/1bwB8A+4DvA3/cLXYj8PikipQ0fsOM4bce2J5kDYM/Fg9X1b8k+RHwUJK/Af4duH+CdUonvKW685bqIpxWN+Cy4a+qPcAlC8x/jcH5v6RVyDv8pEYZfqlRhl9qlOGXGmX4pUalasEb8ybTWPIG8B/d0zOAn/fW+OKs48Os48NWWx2/XVVnDvMLew3/hxpO5qpqdiqNW4d1WIeH/VKrDL/UqGmGf+sU257POj7MOj7sI1vH1M75JU2Xh/1So6YS/iRXJvlJkleT3DqNGro6Xk/yYpLdSeZ6bHdbksNJ9s6bty7JU0le6R5Pn1Idtyf5r26b7E5yVQ91bEzy/ST7kryU5M+7+b1ukyXq6HWbJDk5yTNJXujq+Otu/nlJdnXb4ztJThqpoarq9QdYw2AYsE8DJwEvABf2XUdXy+vAGVNo93PApcDeefP+Fri1m74V+OaU6rgd+Iuet8d64NJu+jTgZeDCvrfJEnX0uk2AAKd20zPALgYD6DwMXNfN/3vgz0ZpZxp7/s3Aq1X1Wg2G+n4IuHoKdUxNVT0NvHnM7KsZDIQKPQ2Iukgdvauqg1X1fDf9NoPBYjbQ8zZZoo5e1cDEB82dRvg3AD+b93yag38W8L0kzyXZMqUajjq7qg7C4E0InDXFWm5Osqc7LZj46cd8Sc5lMH7ELqa4TY6pA3reJn0MmjuN8GeBedPqcri8qi4F/hD4WpLPTamOE8m9wPkMvqPhIHBnXw0nORV4BLilqt7qq90h6uh9m9QIg+YOaxrh3w9snPd80cE/J62qDnSPh4HHmO7IRIeSrAfoHg9Po4iqOtS98T4A7qOnbZJkhkHgHqiqR7vZvW+TheqY1jbp2j7uQXOHNY3wPwtc0F25PAm4Dnii7yKSfCzJaUengS8Be5dea6KeYDAQKkxxQNSjYetcSw/bJEkYjAG5r6rumvdSr9tksTr63ia9DZrb1xXMY65mXsXgSupPgb+cUg2fZtDT8ALwUp91AA8yOHx8l8GR0E3AJ4CdwCvd47op1fGPwIvAHgbhW99DHb/H4BB2D7C7+7mq722yRB29bhPgIgaD4u5h8Ifmr+a9Z58BXgX+GVg7Sjve4Sc1yjv8pEYZfqlRhl9qlOGXGmX4pUYZfqlRhl9qlOGXGvX/mbVe/RflK+4AAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "conway_ker = ker.get_function(\"conway_ker\")\n",
    "if __name__ == '__main__':\n",
    " # set lattice size\n",
    " N = 32\n",
    " lattice = np.int32( np.random.choice([1,0], N*N, p=[0.25, 0.75]).reshape(N, N) )\n",
    " lattice_gpu = gpuarray.to_gpu(lattice)\n",
    " conway_ker(lattice_gpu, np.int32(1000000), grid=(1,1,1), block=(32,32,1))\n",
    " fig = plt.figure(1)\n",
    " plt.imshow(lattice_gpu.get())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pycuda.autoinit\n",
    "import pycuda.driver as drv\n",
    "from pycuda import gpuarray\n",
    "from pycuda.compiler import SourceModule\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt \n",
    "from time import time"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [],
   "source": [
    "shared_ker = SourceModule(\"\"\"    \n",
    "#define _iters 1000000                       \n",
    "#define _X  ( threadIdx.x + blockIdx.x * blockDim.x )\n",
    "#define _Y  ( threadIdx.y + blockIdx.y * blockDim.y )\n",
    "#define _WIDTH  ( blockDim.x * gridDim.x )\n",
    "#define _HEIGHT ( blockDim.y * gridDim.y  )\n",
    "#define _XM(x)  ( (x + _WIDTH) % _WIDTH )\n",
    "#define _YM(y)  ( (y + _HEIGHT) % _HEIGHT )\n",
    "#define _INDEX(x,y)  ( _XM(x)  + _YM(y) * _WIDTH )\n",
    "// return the number of living neighbors for a given cell                \n",
    "__device__ int nbrs(int x, int y, int * in)\n",
    "{\n",
    "     return ( in[ _INDEX(x -1, y+1) ] + in[ _INDEX(x-1, y) ] + in[ _INDEX(x-1, y-1) ] \\\n",
    "                   + in[ _INDEX(x, y+1)] + in[_INDEX(x, y - 1)] \\\n",
    "                   + in[ _INDEX(x+1, y+1) ] + in[ _INDEX(x+1, y) ] + in[ _INDEX(x+1, y-1) ] );\n",
    "}\n",
    "__global__ void conway_ker_shared(int * glob_lattice, int iters)\n",
    "{\n",
    "   // x, y are the appropriate values for the cell covered by this thread\n",
    "   int x = _X, y = _Y;\n",
    "   __shared__ int lattice[32*32];\n",
    "   \n",
    "   \n",
    "   lattice[_INDEX(x,y)] = glob_lattice[_INDEX(x,y)];\n",
    "   __syncthreads();\n",
    "   for (int i = 0; i < iters; i++)\n",
    "   {\n",
    "   \n",
    "       // count the number of neighbors around the current cell\n",
    "       int n = nbrs(x, y, lattice);\n",
    "       \n",
    "       int cell_value;\n",
    "                       \n",
    "        \n",
    "        // if the current cell is alive, then determine if it lives or dies for the next generation.\n",
    "        if ( lattice[_INDEX(x,y)] == 1)\n",
    "           switch(n)\n",
    "           {\n",
    "              // if the cell is alive: it remains alive only if it has 2 or 3 neighbors.\n",
    "              case 2:\n",
    "              case 3: cell_value = 1;\n",
    "                      break;\n",
    "              default: cell_value = 0;                   \n",
    "           }\n",
    "        else if( lattice[_INDEX(x,y)] == 0 )\n",
    "             switch(n)\n",
    "             {\n",
    "                // a dead cell comes to life only if it has 3 neighbors that are alive.\n",
    "                case 3: cell_value = 1;\n",
    "                        break;\n",
    "                default: cell_value = 0;         \n",
    "             }\n",
    "             \n",
    "        __syncthreads();\n",
    "        lattice[_INDEX(x,y)] = cell_value;\n",
    "        __syncthreads();\n",
    "         \n",
    "    }\n",
    "             \n",
    "    __syncthreads();\n",
    "    glob_lattice[_INDEX(x,y)] = lattice[_INDEX(x,y)];\n",
    "    __syncthreads();\n",
    "         \n",
    "}\n",
    "\"\"\")\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAP8AAAD8CAYAAAC4nHJkAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADl0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uIDIuMi41LCBodHRwOi8vbWF0cGxvdGxpYi5vcmcvSM8oowAADSZJREFUeJzt3V2oZfV5x/HvU3ucqW/EqdFMxmk1YiBSdJTDKBiCjW20UlChKXoRvJCcUCJUSC/EQrXQi6RUxSvLWIdMitXYqChFOpEhxeZm9Gh0HDOpGrFxOsOM6SRoK45vTy/2GjgzPS979l5r7XPO8/3AYa+99lrr/7DYv7Pe9vqvyEwk1fMbky5A0mQYfqkowy8VZfilogy/VJThl4oy/FJRhl8qyvBLRf3mODNHxNXAvcAJwD9k5rcXm/7EWJNrOXmcJtWzz1/43kjzvbrrpJYr0TDe53/5IA/HMNPGqD/vjYgTgFeBPwT2As8BN2bmTxea57RYl5fGlSO1p8nYvu/Fkea76rObWq5Ew9iZO3gnDw0V/nF2+zcDr2fmG5n5AfAwcO0Yy5PUo3HCvwF4a877vc04SSvAOMf88+1a/L9jiIiYAWYA1uJxoLRcjLPl3wtsnPP+bGDfsRNl5pbMnM7M6SnWjNGcpDaNE/7ngPMj4tyIOBG4AXiynbIkdW3k3f7M/CgibgG2M7jUtzUzX2mtsuI8y66ujXWdPzOfAp5qqRZJPfIXflJRhl8qyvBLRRl+qSjDLxU11tl+rX5eOly93PJLRRl+qSjDLxVl+KWiDL9UlGf7lynPsqtrbvmlogy/VJThl4oy/FJRhl8qyvBLRRl+qSjDLxVl+KWiDL9UlOGXijL8UlGGXypqrLv6IuJN4F3gY+CjzJxuoyhJ3Wvjlt7fz8xftrAcST1yt18qatzwJ/DDiHg+ImbaKEhSP8bd7b88M/dFxJnA0xHxs8x8Zu4EzT+FGYC1nDRmc5LaMtaWPzP3Na8HgceBzfNMsyUzpzNzeoo14zQnqUUjhz8iTo6IU48MA18BdrdVmKRujbPbfxbweEQcWc4/Zea/tlKVpM6NHP7MfAO4qMVaJPXIS31SUYZfKsrwS0UZfqkowy8VZfilogy/VJThl4oy/FJRhl8qyvBLRRl+qSjDLxVl+KWiDL9UlOGXijL8UlGGXyrK8EtFGX6pKMMvFWX4paIMv1SU4ZeKMvxSUUuGPyK2RsTBiNg9Z9y6iHg6Il5rXk/vtkxJbRtmy/9d4Opjxt0G7MjM84EdzXtJK8iS4c/MZ4BDx4y+FtjWDG8Drmu5LkkdG/WY/6zM3A/QvJ7ZXkmS+jDOI7qHEhEzwAzAWk7qujlJQxp1y38gItYDNK8HF5owM7dk5nRmTk+xZsTmJLVt1PA/CdzUDN8EPNFOOZL6suRuf0Q8BFwBnBERe4E7gG8Dj0TEzcAvgK92WaSko23f9+K84zdf9d7Qy1gy/Jl54wIfXTl0K5KWHX/hJxVl+KWiDL9UlOGXijL8UlGd/8Jvrs9f+B7bt89/ieKqz27qsxRp2Vvoch4snJdX87+HXr5bfqkowy8VZfilogy/VJThl4oy/FJRhl8qyvBLRRl+qSjDLxVl+KWiDL9UVGRmb42dFuvy0rD3L2lcC/fh9xazL70fwyzDLb9UlOGXijL8UlGGXyrK8EtFGX6pqGEe17UV+GPgYGb+XjPuTuDrwNvNZLdn5lNdFSnpaH314fdd4Op5xt+TmZuaP4MvrTBLhj8znwEO9VCLpB6Nc8x/S0TsioitEXF6axVJ6sWo4b8POA/YBOwH7lpowoiYiYjZiJj9kMMjNiepbSOFPzMPZObHmfkJcD+weZFpt2TmdGZOT7Fm1DoltWyk8EfE+jlvrwd2t1OOpL4Mc6nvIeAK4IyI2AvcAVwREZuABN4EvtFhjQta7HFGi/HRYBrVavrOLRn+zLxxntEPdFCLpB75Cz+pKMMvFWX4paIMv1SU4ZeKWvJs/3Kw0OWVUS+fLHa5ZjleklG/uvh+LMfvnFt+qSjDLxVl+KWiDL9UlOGXijL8UlEr4lLfQpdCVtMdVlo+Fvt+rKbvnFt+qSjDLxVl+KWiDL9UlOGXiloRZ/sXshzPoGp1W03fObf8UlGGXyrK8EtFGX6pKMMvFWX4paKGeVzXRuB7wGeAT4AtmXlvRKwDvg+cw+CRXX+amb/qrlRJRyx0g9Hmq94behnDbPk/Ar6VmV8ALgO+GREXALcBOzLzfGBH817SCrFk+DNzf2a+0Ay/C+wBNgDXAtuaybYB13VVpKT2Hdcxf0ScA1wM7ATOysz9MPgHAZzZdnGSujN0+CPiFOBR4NbMfOc45puJiNmImP2Qw6PUKKkDQ4U/IqYYBP/BzHysGX0gItY3n68HDs43b2ZuyczpzJyeYk0bNUtqwZLhj4gAHgD2ZObdcz56EripGb4JeKL98iR1JTJz8Qkivgj8O/Ayg0t9ALczOO5/BPgd4BfAVzPz0GLLmr5obT67feO8n62mu6WkNozyiK+duYN38lAMs/wlr/Nn5o+BhRZ25TCNSFp+/IWfVJThl4oy/FJRhl8qyvBLRRl+qSjDLxVl+KWiDL9UlOGXijL8UlGGXypqybv62nRarMtLw3uBdPwWu8NtFCv9LtKFO/B8i9mX3h/qrj63/FJRhl8qyvBLRRl+qSjDLxXl2X5pFTmePvzc8ktFGX6pKMMvFWX4paIMv1SU4ZeKWvKJPRGxEfge8BkGj+vakpn3RsSdwNeBt5tJb8/Mp7oqVKvDKI+g6ttKqLENS4Yf+Aj4Vma+EBGnAs9HxNPNZ/dk5t91V56krgzzrL79wP5m+N2I2ANs6LowSd06rmP+iDgHuJjBE3oBbomIXRGxNSJOb7k2SR0aOvwRcQrwKHBrZr4D3AecB2xisGdw1wLzzUTEbETMfsjhFkqW1Iahwh8RUwyC/2BmPgaQmQcy8+PM/AS4H9g837yZuSUzpzNzeoo1bdUtaUxLhj8iAngA2JOZd88Zv37OZNcDu9svT1JXhjnbfznwNeDliDhyDeR24MaI2AQk8CbwjU4qlNSJYc72/xiY7xZBr+lLK5i/8JOKMvxSUYZfKsrwS0UZfqkowy8VZfilogy/VJThl4oy/FJRhl8qyvBLRQ1zV5/UmpXQAeZKqLENbvmlogy/VJThl4oy/FJRhl8qyvBLRXmpTyvCYs/PG0WVy3mLccsvFWX4paIMv1SU4ZeKMvxSUUue7Y+ItcAzwJpm+h9k5h0RcS7wMLAOeAH4WmZ+0GWxqsuz8+0bZst/GPhyZl7E4HHcV0fEZcB3gHsy83zgV8DN3ZUpqW1Lhj8H/qd5O9X8JfBl4AfN+G3AdZ1UKKkTQx3zR8QJzRN6DwJPAz8Hfp2ZHzWT7AU2dFOipC4MFf7M/DgzNwFnA5uBL8w32XzzRsRMRMxGxOyHHB69UkmtOq6z/Zn5a+DfgMuAT0XEkROGZwP7FphnS2ZOZ+b0FGvGqVVSi5YMf0R8OiI+1Qz/FvAHwB7gR8CfNJPdBDzRVZGS2jfMjT3rgW0RcQKDfxaPZOa/RMRPgYcj4m+AnwAPdFinpJYtGf7M3AVcPM/4Nxgc/0tagfyFn1SU4ZeKMvxSUYZfKsrwS0VF5rw/zOumsYi3gf9s3p4B/LK3xhdmHUezjqOttDp+NzM/PcwCew3/UQ1HzGbm9EQatw7rsA53+6WqDL9U1CTDv2WCbc9lHUezjqOt2jomdswvabLc7ZeKmkj4I+LqiPiPiHg9Im6bRA1NHW9GxMsR8WJEzPbY7taIOBgRu+eMWxcRT0fEa83r6ROq486I+K9mnbwYEdf0UMfGiPhRROyJiFci4s+b8b2uk0Xq6HWdRMTaiHg2Il5q6vjrZvy5EbGzWR/fj4gTx2ooM3v9A05g0A3Y54ATgZeAC/quo6nlTeCMCbT7JeASYPeccX8L3NYM3wZ8Z0J13An8Rc/rYz1wSTN8KvAqcEHf62SROnpdJ0AApzTDU8BOBh3oPALc0Iz/e+DPxmlnElv+zcDrmflGDrr6fhi4dgJ1TExmPgMcOmb0tQw6QoWeOkRdoI7eZeb+zHyhGX6XQWcxG+h5nSxSR69yoPNOcycR/g3AW3PeT7LzzwR+GBHPR8TMhGo44qzM3A+DLyFw5gRruSUidjWHBZ0ffswVEecw6D9iJxNcJ8fUAT2vkz46zZ1E+GOecZO65HB5Zl4C/BHwzYj40oTqWE7uA85j8IyG/cBdfTUcEacAjwK3ZuY7fbU7RB29r5Mco9PcYU0i/HuBjXPeL9j5Z9cyc1/zehB4nMn2THQgItYDNK8HJ1FEZh5ovnifAPfT0zqJiCkGgXswMx9rRve+TuarY1LrpGn7uDvNHdYkwv8ccH5z5vJE4Abgyb6LiIiTI+LUI8PAV4Ddi8/VqScZdIQKE+wQ9UjYGtfTwzqJiGDQB+SezLx7zke9rpOF6uh7nfTWaW5fZzCPOZt5DYMzqT8H/nJCNXyOwZWGl4BX+qwDeIjB7uOHDPaEbgZ+G9gBvNa8rptQHf8IvAzsYhC+9T3U8UUGu7C7gBebv2v6XieL1NHrOgEuZNAp7i4G/2j+as539lngdeCfgTXjtOMv/KSi/IWfVJThl4oy/FJRhl8qyvBLRRl+qSjDLxVl+KWi/g8GlZVRV/L63wAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.000205993652344\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAP8AAAD8CAYAAAC4nHJkAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADl0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uIDIuMi41LCBodHRwOi8vbWF0cGxvdGxpYi5vcmcvSM8oowAADSZJREFUeJzt3V2oZfV5x/HvU3ucqW/EqdFMxmk1YiBSdJTDKBiCjW20UlChKXoRvJCcUCJUSC/EQrXQi6RUxSvLWIdMitXYqChFOpEhxeZm9Gh0HDOpGrFxOsOM6SRoK45vTy/2GjgzPS979l5r7XPO8/3AYa+99lrr/7DYv7Pe9vqvyEwk1fMbky5A0mQYfqkowy8VZfilogy/VJThl4oy/FJRhl8qyvBLRf3mODNHxNXAvcAJwD9k5rcXm/7EWJNrOXmcJtWzz1/43kjzvbrrpJYr0TDe53/5IA/HMNPGqD/vjYgTgFeBPwT2As8BN2bmTxea57RYl5fGlSO1p8nYvu/Fkea76rObWq5Ew9iZO3gnDw0V/nF2+zcDr2fmG5n5AfAwcO0Yy5PUo3HCvwF4a877vc04SSvAOMf88+1a/L9jiIiYAWYA1uJxoLRcjLPl3wtsnPP+bGDfsRNl5pbMnM7M6SnWjNGcpDaNE/7ngPMj4tyIOBG4AXiynbIkdW3k3f7M/CgibgG2M7jUtzUzX2mtsuI8y66ujXWdPzOfAp5qqRZJPfIXflJRhl8qyvBLRRl+qSjDLxU11tl+rX5eOly93PJLRRl+qSjDLxVl+KWiDL9UlGf7lynPsqtrbvmlogy/VJThl4oy/FJRhl8qyvBLRRl+qSjDLxVl+KWiDL9UlOGXijL8UlGGXypqrLv6IuJN4F3gY+CjzJxuoyhJ3Wvjlt7fz8xftrAcST1yt18qatzwJ/DDiHg+ImbaKEhSP8bd7b88M/dFxJnA0xHxs8x8Zu4EzT+FGYC1nDRmc5LaMtaWPzP3Na8HgceBzfNMsyUzpzNzeoo14zQnqUUjhz8iTo6IU48MA18BdrdVmKRujbPbfxbweEQcWc4/Zea/tlKVpM6NHP7MfAO4qMVaJPXIS31SUYZfKsrwS0UZfqkowy8VZfilogy/VJThl4oy/FJRhl8qyvBLRRl+qSjDLxVl+KWiDL9UlOGXijL8UlGGXyrK8EtFGX6pKMMvFWX4paIMv1SU4ZeKMvxSUUuGPyK2RsTBiNg9Z9y6iHg6Il5rXk/vtkxJbRtmy/9d4Opjxt0G7MjM84EdzXtJK8iS4c/MZ4BDx4y+FtjWDG8Drmu5LkkdG/WY/6zM3A/QvJ7ZXkmS+jDOI7qHEhEzwAzAWk7qujlJQxp1y38gItYDNK8HF5owM7dk5nRmTk+xZsTmJLVt1PA/CdzUDN8EPNFOOZL6suRuf0Q8BFwBnBERe4E7gG8Dj0TEzcAvgK92WaSko23f9+K84zdf9d7Qy1gy/Jl54wIfXTl0K5KWHX/hJxVl+KWiDL9UlOGXijL8UlGd/8Jvrs9f+B7bt89/ieKqz27qsxRp2Vvoch4snJdX87+HXr5bfqkowy8VZfilogy/VJThl4oy/FJRhl8qyvBLRRl+qSjDLxVl+KWiDL9UVGRmb42dFuvy0rD3L2lcC/fh9xazL70fwyzDLb9UlOGXijL8UlGGXyrK8EtFGX6pqGEe17UV+GPgYGb+XjPuTuDrwNvNZLdn5lNdFSnpaH314fdd4Op5xt+TmZuaP4MvrTBLhj8znwEO9VCLpB6Nc8x/S0TsioitEXF6axVJ6sWo4b8POA/YBOwH7lpowoiYiYjZiJj9kMMjNiepbSOFPzMPZObHmfkJcD+weZFpt2TmdGZOT7Fm1DoltWyk8EfE+jlvrwd2t1OOpL4Mc6nvIeAK4IyI2AvcAVwREZuABN4EvtFhjQta7HFGi/HRYBrVavrOLRn+zLxxntEPdFCLpB75Cz+pKMMvFWX4paIMv1SU4ZeKWvJs/3Kw0OWVUS+fLHa5ZjleklG/uvh+LMfvnFt+qSjDLxVl+KWiDL9UlOGXijL8UlEr4lLfQpdCVtMdVlo+Fvt+rKbvnFt+qSjDLxVl+KWiDL9UlOGXiloRZ/sXshzPoGp1W03fObf8UlGGXyrK8EtFGX6pKMMvFWX4paKGeVzXRuB7wGeAT4AtmXlvRKwDvg+cw+CRXX+amb/qrlRJRyx0g9Hmq94behnDbPk/Ar6VmV8ALgO+GREXALcBOzLzfGBH817SCrFk+DNzf2a+0Ay/C+wBNgDXAtuaybYB13VVpKT2Hdcxf0ScA1wM7ATOysz9MPgHAZzZdnGSujN0+CPiFOBR4NbMfOc45puJiNmImP2Qw6PUKKkDQ4U/IqYYBP/BzHysGX0gItY3n68HDs43b2ZuyczpzJyeYk0bNUtqwZLhj4gAHgD2ZObdcz56EripGb4JeKL98iR1JTJz8Qkivgj8O/Ayg0t9ALczOO5/BPgd4BfAVzPz0GLLmr5obT67feO8n62mu6WkNozyiK+duYN38lAMs/wlr/Nn5o+BhRZ25TCNSFp+/IWfVJThl4oy/FJRhl8qyvBLRRl+qSjDLxVl+KWiDL9UlOGXijL8UlGGXypqybv62nRarMtLw3uBdPwWu8NtFCv9LtKFO/B8i9mX3h/qrj63/FJRhl8qyvBLRRl+qSjDLxXl2X5pFTmePvzc8ktFGX6pKMMvFWX4paIMv1SU4ZeKWvKJPRGxEfge8BkGj+vakpn3RsSdwNeBt5tJb8/Mp7oqVKvDKI+g6ttKqLENS4Yf+Aj4Vma+EBGnAs9HxNPNZ/dk5t91V56krgzzrL79wP5m+N2I2ANs6LowSd06rmP+iDgHuJjBE3oBbomIXRGxNSJOb7k2SR0aOvwRcQrwKHBrZr4D3AecB2xisGdw1wLzzUTEbETMfsjhFkqW1Iahwh8RUwyC/2BmPgaQmQcy8+PM/AS4H9g837yZuSUzpzNzeoo1bdUtaUxLhj8iAngA2JOZd88Zv37OZNcDu9svT1JXhjnbfznwNeDliDhyDeR24MaI2AQk8CbwjU4qlNSJYc72/xiY7xZBr+lLK5i/8JOKMvxSUYZfKsrwS0UZfqkowy8VZfilogy/VJThl4oy/FJRhl8qyvBLRQ1zV5/UmpXQAeZKqLENbvmlogy/VJThl4oy/FJRhl8qyvBLRXmpTyvCYs/PG0WVy3mLccsvFWX4paIMv1SU4ZeKMvxSUUue7Y+ItcAzwJpm+h9k5h0RcS7wMLAOeAH4WmZ+0GWxqsuz8+0bZst/GPhyZl7E4HHcV0fEZcB3gHsy83zgV8DN3ZUpqW1Lhj8H/qd5O9X8JfBl4AfN+G3AdZ1UKKkTQx3zR8QJzRN6DwJPAz8Hfp2ZHzWT7AU2dFOipC4MFf7M/DgzNwFnA5uBL8w32XzzRsRMRMxGxOyHHB69UkmtOq6z/Zn5a+DfgMuAT0XEkROGZwP7FphnS2ZOZ+b0FGvGqVVSi5YMf0R8OiI+1Qz/FvAHwB7gR8CfNJPdBDzRVZGS2jfMjT3rgW0RcQKDfxaPZOa/RMRPgYcj4m+AnwAPdFinpJYtGf7M3AVcPM/4Nxgc/0tagfyFn1SU4ZeKMvxSUYZfKsrwS0VF5rw/zOumsYi3gf9s3p4B/LK3xhdmHUezjqOttDp+NzM/PcwCew3/UQ1HzGbm9EQatw7rsA53+6WqDL9U1CTDv2WCbc9lHUezjqOt2jomdswvabLc7ZeKmkj4I+LqiPiPiHg9Im6bRA1NHW9GxMsR8WJEzPbY7taIOBgRu+eMWxcRT0fEa83r6ROq486I+K9mnbwYEdf0UMfGiPhRROyJiFci4s+b8b2uk0Xq6HWdRMTaiHg2Il5q6vjrZvy5EbGzWR/fj4gTx2ooM3v9A05g0A3Y54ATgZeAC/quo6nlTeCMCbT7JeASYPeccX8L3NYM3wZ8Z0J13An8Rc/rYz1wSTN8KvAqcEHf62SROnpdJ0AApzTDU8BOBh3oPALc0Iz/e+DPxmlnElv+zcDrmflGDrr6fhi4dgJ1TExmPgMcOmb0tQw6QoWeOkRdoI7eZeb+zHyhGX6XQWcxG+h5nSxSR69yoPNOcycR/g3AW3PeT7LzzwR+GBHPR8TMhGo44qzM3A+DLyFw5gRruSUidjWHBZ0ffswVEecw6D9iJxNcJ8fUAT2vkz46zZ1E+GOecZO65HB5Zl4C/BHwzYj40oTqWE7uA85j8IyG/cBdfTUcEacAjwK3ZuY7fbU7RB29r5Mco9PcYU0i/HuBjXPeL9j5Z9cyc1/zehB4nMn2THQgItYDNK8HJ1FEZh5ovnifAPfT0zqJiCkGgXswMx9rRve+TuarY1LrpGn7uDvNHdYkwv8ccH5z5vJE4Abgyb6LiIiTI+LUI8PAV4Ddi8/VqScZdIQKE+wQ9UjYGtfTwzqJiGDQB+SezLx7zke9rpOF6uh7nfTWaW5fZzCPOZt5DYMzqT8H/nJCNXyOwZWGl4BX+qwDeIjB7uOHDPaEbgZ+G9gBvNa8rptQHf8IvAzsYhC+9T3U8UUGu7C7gBebv2v6XieL1NHrOgEuZNAp7i4G/2j+as539lngdeCfgTXjtOMv/KSi/IWfVJThl4oy/FJRhl8qyvBLRRl+qSjDLxVl+KWi/g8GlZVRV/L63wAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.000265121459961\n"
     ]
    }
   ],
   "source": [
    "conway_ker_shared = shared_ker.get_function(\"conway_ker_shared\")\n",
    "conway_ker = ker.get_function(\"conway_ker\")\n",
    "if __name__ == '__main__':\n",
    "    # set lattice size\n",
    "    N = 32    \n",
    "    lattice = np.int32( np.random.choice([1,0], N*N, p=[0.25, 0.75]).reshape(N, N) )\n",
    "    lattice_gpu = gpuarray.to_gpu(lattice)\n",
    "    lattice_gpu_shared = gpuarray.to_gpu(lattice)\n",
    "    t1=time()\n",
    "    conway_ker_shared(lattice_gpu, np.int32(1000000), grid=(1,1,1), block=(32,32,1))      \n",
    "    t2 = time()\n",
    "    fig = plt.figure(1)\n",
    "    plt.imshow(lattice_gpu.get())\n",
    "    plt.show()\n",
    "    print(t2-t1)\n",
    "    t3=time()\n",
    "    conway_ker(lattice_gpu_shared, np.int32(1000000), grid=(1,1,1), block=(32,32,1))\n",
    "    t4 = time()\n",
    "    plt.imshow(lattice_gpu_shared.get())\n",
    "    plt.show()\n",
    "    print(t4-t3)\n",
    "    ### Compare shared and global memory\n",
    "    #First result is shared second is using global"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 100,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pycuda.autoinit\n",
    "import pycuda.driver as drv\n",
    "import numpy as np\n",
    "from pycuda import gpuarray\n",
    "from pycuda.compiler import SourceModule\n",
    "from time import time\n",
    "# this is a naive parallel prefix-sum kernel that uses shared memory\n",
    "naive_ker = SourceModule(\"\"\"\n",
    "__global__ void naive_prefix(double *vec, double *out)\n",
    "{\n",
    "     __shared__ double sum_buf[1024];     \n",
    "     int tid = threadIdx.x;     \n",
    "     sum_buf[tid] = vec[tid];\n",
    "     \n",
    "     // begin parallel prefix sum algorithm\n",
    "     \n",
    "     int iter = 1;\n",
    "     for (int i=0; i < 10; i++)\n",
    "     {\n",
    "         __syncthreads();\n",
    "         if (tid >= iter )\n",
    "         {\n",
    "             sum_buf[tid] = sum_buf[tid] + sum_buf[tid - iter];            \n",
    "         }\n",
    "         \n",
    "         iter *= 2;\n",
    "     }\n",
    "         \n",
    "    __syncthreads();\n",
    "    out[tid] = sum_buf[tid];\n",
    "    __syncthreads();\n",
    "        \n",
    "}\n",
    "\"\"\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 101,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(array(-72.58195917644991), -72.581959176449857)\n",
      "Does our kernel work correctly? : True\n"
     ]
    }
   ],
   "source": [
    "naive_gpu = naive_ker.get_function(\"naive_prefix\")\n",
    "if __name__ == '__main__':\n",
    "    \n",
    "    \n",
    "    testvec = np.random.randn(1024).astype(np.float64)\n",
    "    testvec_gpu = gpuarray.to_gpu(testvec)\n",
    "    \n",
    "    outvec_gpu = gpuarray.empty_like(testvec_gpu)\n",
    "\n",
    "    naive_gpu( testvec_gpu , outvec_gpu, block=(1024,1,1), grid=(1,1,1))\n",
    "    \n",
    "    total_sum = sum( testvec)\n",
    "    total_sum_gpu = outvec_gpu[-1].get()\n",
    "    print(total_sum_gpu, total_sum)\n",
    "    print \"Does our kernel work correctly? : {}\".format(np.allclose(total_sum_gpu , total_sum) )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pycuda.autoinit\n",
    "import pycuda.driver as drv\n",
    "import numpy as np\n",
    "from pycuda import gpuarray\n",
    "from pycuda.compiler import SourceModule\n",
    "from time import time\n",
    "# this is a naive parallel prefix-sum kernel that uses shared memory\n",
    "naive_ker_int = SourceModule(\"\"\"\n",
    "__global__ void naive_prefix_int(int *vec, int *out)\n",
    "{\n",
    "     __shared__ int sum_buf[1024];     \n",
    "     int tid = threadIdx.x;     \n",
    "     sum_buf[tid] = vec[tid];\n",
    "     \n",
    "     // begin parallel prefix sum algorithm\n",
    "     \n",
    "     int iter = 1;\n",
    "     for (int i=0; i < 10; i++)\n",
    "     {\n",
    "         __syncthreads();\n",
    "         if (tid >= iter )\n",
    "         {\n",
    "             sum_buf[tid] = sum_buf[tid] + sum_buf[tid - iter];            \n",
    "         }\n",
    "         \n",
    "         iter *= 2;\n",
    "     }\n",
    "        \n",
    "    __syncthreads();\n",
    "    out[tid] = sum_buf[tid];\n",
    "    __syncthreads();\n",
    "        \n",
    "}\n",
    "\"\"\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "129286\n",
      "(131328, array(131328, dtype=int32), 131328)\n",
      "GPU time: 0.00056100, CPU time sum: 0.00052404 , Sum using integer summation: 0.00044584\n"
     ]
    }
   ],
   "source": [
    "N = 512\n",
    "naive_gpu = naive_ker_int.get_function(\"naive_prefix_int\")\n",
    "testvec2 = np.array(range(1,N+1),dtype=np.intc)\n",
    "testvec2_gpu = gpuarray.to_gpu(testvec2)\n",
    "outvec2_gpu = gpuarray.empty_like(testvec2_gpu)\n",
    "t1=time()\n",
    "naive_gpu( testvec2_gpu , outvec2_gpu, block=(N,1,1), grid=(1,1,1))\n",
    "t2=time()\n",
    "t3=time()\n",
    "total_sum = sum(testvec2)\n",
    "t4=time()\n",
    "def sequential_integer_sum(x):\n",
    "    N=len(x)\n",
    "    return(N*(N+1)/2)\n",
    "t5 = time()\n",
    "total_sum2 = sequential_integer_sum(testvec2)\n",
    "t6 = time()\n",
    "total_sum_gpu2 = outvec2_gpu[-1].get()\n",
    "print(outvec2_gpu.get()[-5])\n",
    "print(total_sum, total_sum_gpu2, total_sum2)\n",
    "if( total_sum_gpu2 == total_sum2 and total_sum2 == total_sum):\n",
    "    print(\"GPU time: {:.8f}, CPU time sum: {:.8f} , Sum using integer summation: {:.8f}\".format((t2-t1),(t4-t3),(t6-t4)))\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 98,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "512"
      ]
     },
     "execution_count": 98,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "testvec2[-1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.17"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
